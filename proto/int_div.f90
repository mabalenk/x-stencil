
!>    @brief Tests integer division in Fortran 90
!!
!!    @author  Maksims Abalenkovs
!!    @email   m.abalenkovs@manchester.ac.uk
!!    @date    Aug 23, 2017
!!    @version 0.1

      PROGRAM INT_DIV

      integer          :: a1 = 24
      double precision :: a2 = 24.0d+0
      double precision :: b  = 100.0d+0, c1 = 0.0d+0, c2 = 0.0d+0

      print *, "a1, a2, b:", a1, a2, b

      c1 = b/a1
      c2 = b/a2

      print *, "c1, c2:", c1, c2

      END PROGRAM INT_DIV

!-----------------------------------------------------------------------
!     @eof int_div.f90
!-----------------------------------------------------------------------
