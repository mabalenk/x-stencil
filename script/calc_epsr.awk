#!/opt/local/bin/gawk -f
#
# @brief Calculates relative electric permittivity 'eps_r' from one pole Debye
#        relaxation data
#
# @author  Maksims Abalenkovs
# @email   m.abalenkovs@manchester.ac.uk
# @date    Apr 4, 2017
# @version 2cae7e

BEGIN {
  FPAT = "([^ ]+)|(\"[^\"]+\")"
}
{
  if ($1 ~ /^[[:digit:]]/) {

    mcode=$1
    mdesc=$2
    sigma=$3
    eps_s=$4
    eps_i=$5
    tau_1=$6

    # printf "%d %s\t%.8f\t%.8f\t%.8f\t%.8f\n", mcode, mdesc, sigma, eps_s, eps_i, tau_1

    eps_r = eps_i + sigma/img/afreq/eps_0 + (eps_s-eps_i)/(1+img*afreq*tau_1)

    printf "%d %s\t%.8f\t%.8f\n", mcode, mdesc, sigma, eps_r

  }
}
END {
  # Output footer
  printf "\n%s\t%s\t%s\t%s\t%s\t%s\n", "! <mcode>", "<mdesc>", "<sigma>", "<eps_s>", "<eps_i>", "<tau_1>"
}
