% @brief Script to solve linear system to find out coefficients A, B, C for
%        8th order finite-difference approximations
%
% @author  Maksims Abalenkovs
% @email   m.abalenkovs@manchester.ac.uk
% @date    Jul 28, 2017
% @version 0.1

l22 = (3^4-1)/(3^2-1)

alpha = 7^3*( (-7^4+1)-l22*(-7^2+1) )

beta  = 5^3*( ( 5^4-1)-l22*( 5^2-1) )

C = rats(alpha/beta)

B = rats( (-7^5 + 7^3 + (5^5 - 5^3) * 7^3 / 5^2) / (3^5 - 3^3) )

A = -7^3 + 5^3 * 7^3/5^2 - 3^3 * 7^3/3

a = -7^3*5
b =  7^3/3
c = -7^3/5^2

M = [1 1 1 1;  3 3^3 3^5 3^7;  5 5^3 5^5 5^7;  7 7^3 7^5 7^7]
v = [a b c 1]

v*M
