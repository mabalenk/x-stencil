#!/bin/bash

# @brief Bash script to run multiple OpenMP experiments and collect data.
#
# @author  Maksims Abalenkovs
# @email   m.abalenkovs@manchester.ac.uk
# @date    Apr 4, 2017
# @version 

# use FDTD coefficients in pre-computed matrix form
# preflags=("F" "T")
preflags=("F")

# types of FDTD update equations
# eqtypes=("expl" "func" "dotp" "matm")
eqtypes=("expl")

# number of threads
# threads=(1 2 3 4 5 6 7 8)
threads=(8)

# length of threads array
nthreads=${#threads[@]}

# number of experiments
nexp=1

for preflag in "${preflags[@]}"
do

  if [ "$preflag" == "T" ]
  then
    echo -e "\nUsing pre-computed FDTD coefficients in matrix form..."
    presfx="pre"
  else
    echo -e "\nUsing standard material encodings array..."
    presfx="fly"
  fi

  sed -i -e "s/pre_flag.*/pre_flag: $preflag/g" input_params

  for eqtype in "${eqtypes[@]}"
  do
  
    echo -e "\nUsing $eqtype FDTD update equation..."
  
    sed -i -e "s/eqtype.*/eqtype: $eqtype/g" input_params
  
    for (( i=0; i<$nthreads; i++ ))
    do
    
      echo -e "\nRunning experiment with ${threads[$i]} threads..."
    
      for (( j=0; j<$nexp; j++ ))
      do
    
        OMP_NUM_THREADS=${threads[$i]} PRESFX=$presfx EQTYPE=$eqtype make rerun
    
      done
    
    done
  
  done

done

echo -e "\nAll experiments finished successfully. Exiting..."

# EoF: batch_omp.sh
