% @brief Script to solve linear system to find out coefficients y1 and y2 for
%        6th order finite-difference approximations
%
% @author  Maksims Abalenkovs
% @email   m.abalenkovs@manchester.ac.uk
% @date    Aug 7, 2017
% @version 0.1

C = [1 1; 3^3 3^5]
b = [-5^3 -5^5]

Ct = C'
bt = b'

y = rats(Ct\bt)


Y =[250 -5^3/3^2 1]
C1=[1; 3; 5]

b1 = rats(Y*C1)

C = [C1; 1 1 1; 3 3^3 3^5; 5 5^3 5^5]
b = [1 0 0]
% 
% Ct = C'
% bt = b'
% 
% y = rats(Ct\bt)
