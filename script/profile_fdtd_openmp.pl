#!/usr/local/bin/perl

# @brief   Profiles Fortran implementation of FDTD with various number of OpenMP threads
#
# @author  Maksims Abalenkovs
# @email   maksims.abalenkovs@stfc.ac.uk
# @date    October 31, 2023
# @version 0.2

use v5.38;
use autodie;
use strict;
use warnings;
use IPC::System::Simple qw(systemx);

# FDTD executable
my $fdtd_cmd = './x-stencil';

# GNU time command
my $time_cmd ='/opt/local/libexec/gnubin/time';

# maximum number of OpenMP threads available
use constant NTHREADS => 16;

# log file name
my $logfile = "fdtd.log";
# printf "Log file name: %s\n", $logfile;

# log file header
my $hdr ="# nthr\tt_real\tt_sys\tt_usr\tCPU, %\tmax_rss, KB\tpage_faults_maj\tpage_faults_min\tcontext_inv\tcontext_vol\n";
# printf "Log file header: %s\n", $hdr;

# open log file
open my $fh, ">:encoding(UTF-8)", $logfile;

# print header into log file
print $fh $hdr;

# for each number of threads
for (my $nthr=2; $nthr <=NTHREADS; $nthr*=2) {

    # set number of OpenMP threads
    $ENV{'OMP_NUM_THREADS'} = $nthr;
    printf "OMP_NUM_THREADS: %d\n", $ENV{'OMP_NUM_THREADS'};

    # append number of OpenMP threads to GNU time format string
    my $time_fmt ="-f$nthr\t%e\t%S\t%U\t%P\t%M\t%F\t%R\t%c\t%w";

    # execute FDTD with given number of OpenMP threads
    # append output of GNU time to log file
    systemx($time_cmd, $time_fmt, '-a', '-o', $logfile, $fdtd_cmd);
}

# close log file
close $fh;

# @eof profile_fdtd_openmp.pl
