% @brief Script to solve linear system to find out coefficients A, B, C, D for
%        10th order finite-difference approximations
%
% @author  Maksims Abalenkovs
% @email   m.abalenkovs@manchester.ac.uk
% @date    Jul 30, 2017
% @version 0.3

A = [1 3^3 5^3 7^3;  1 3^5 5^5 7^5;  1 3^7 5^7 7^7;  1 3^9 5^9 7^9]
b = [-9^3; -9^5; -9^7; -9^9]
x = rats(A\b)

% x1 = 10206
% x2 =  -756
% x3 =  2916/25
% x4 =  -729/49
