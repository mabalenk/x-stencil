set terminal postscript eps enhanced colour solid 22;  # font "Helvetica, 22";
# set terminal png enhanced;

set lmargin 12;
set key top Left;
set format "%g";
set output "xstncl-nthr-time-bst-w2.eps";
# set title  "OpenMP Threads vs Execution Time \\@ Wonder2";
set xlabel "No. of threads, n_{thr}";
set ylabel "Execution time, t (s)";
set grid;
# set logscale x;

plot [:][:500] "omp.time.fly.expl" u ($1):($3) t "expl fly" w lp lw 5 ps 1.25, \
               "omp.time.fly.func" u ($1):($3) t "func fly" w lp lw 5 ps 1.25, \
               "omp.time.fly.dotp" u ($1):($3) t "dotp fly" w lp lw 5 ps 1.25, \
               "omp.time.pre.expl" u ($1):($3) t "expl pre" w lp lw 5 ps 1.25, \
               "omp.time.pre.func" u ($1):($3) t "func pre" w lp lw 5 ps 1.25, \
               "omp.time.pre.dotp" u ($1):($3) t "dotp pre" w lp lw 5 ps 1.25, \
               "omp.time.pre.matm" u ($1):($3) t "matm pre" w lp lw 5 ps 1.25;
               # "omp.time.fly.matm" u ($1):($3) t "matm fly" w lp lw 5 ps 1.25, \
