!-----------------------------------------------------------------------
!
!>    @brief Contains implementation of 3D FDTD stencil calculation
!!           routines using standard Yee's 2nd order spatial approximations.
!!
!!    @author  Maksims Abalenkovs
!!    @email   maksims.abalenkovs@stfc.ac.uk
!!    @date    Oct 31, 2023
!!    @version 0.13
!!
!!    Subroutines (8):
!!
!!        mat_coeffs()
!!        calc_mat_coeffs()
!!        build_coeff_mtrx()
!!        calc_E_src()
!!        calc_E_star()
!!        calc_E_star_pre()
!!        calc_H_star()
!!        calc_H_star_pre()
!!
!!    Functions (4):
!!
!!        point_src()
!!        calc_E()
!!        calc_H()
!!        calc_dt()
!
!-----------------------------------------------------------------------

      MODULE YEE

      USE ALLOC
      USE IO
      USE VARIABLES

      IMPLICIT NONE

!-----------------------------------------------------------------------

      CONTAINS

!-----------------------------------------------------------------------

!>    Calculates material coefficients for given electric properties
!!    sigma and relative electric permittivity
!!
!!    @param[in] sigma electric conductivity
!!    @param[in] eps_r relative electric permittivity
!!    @param[in] eps_0 electric permittivity of vacuum
!!    @param[in] dt    temporal increment
!!    @param[in] iota  normalisation parameter for G2
!!    @param[in] G     coefficients for electric field calculation

      SUBROUTINE mat_coeffs(sigma, eps_r, eps_0, dt, iota, G)

      real(p), intent(in)    :: sigma, eps_0, eps_r
      real(p), intent(in)    :: dt, iota
      real(p), intent(inout) :: G(1:)

!     Auxiliary coefficients
      real(p) :: alp, bet, phi, chi


      alp = sigma / 2.0
      bet = dt / eps_0 / eps_r

      phi = alp * bet
      chi = 1.0 + phi

      G(1) = (1-phi) / chi
      G(2) = bet / chi * iota

      END SUBROUTINE mat_coeffs

!-----------------------------------------------------------------------

!>    Calculates material coefficients
!!
!!    @param[in]  dt    temporal increment
!!    @param[in]  iota  normalisation parameter for G2, Z2
!!    @param[in]  nmats no. of materials
!!    @param[out] G     coefficients for electric field calculation
!!    @param[out] Z     coefficients for magnetic field calculation

      SUBROUTINE calc_mat_coeffs(dt, iota, nmats, G, Z)

      real(p),   intent(in)  :: dt, iota
      integer,   intent(in)  :: nmats
      real(p),   intent(out) :: G(1:,1:), Z(1:,1:)

!     Material counter
      integer :: v


      print *, "Calculating material coefficients..."

!     Calculate material coefficients for each material
      do v = 1, nmats

!       Calculate coefficients for electric field
        call mat_coeffs( mats(v)%sigma, mats(v)%eps_r, &
                         eps_0, dt, iota, G(v,:) )

!       Calculate coefficients for magnetic field
        call mat_coeffs( mats(v)%rho, mats(v)%mu_r,   &
                         mu_0, dt, iota**(-1), Z(v,:) )
      end do

!>    @test Output material coefficients
      if (verbose_flag) then

        call output_mat_coeffs(53, "coef.a", G, Z, nmats)

      end if

      END SUBROUTINE calc_mat_coeffs

!-----------------------------------------------------------------------

!>    @brief Builds and saves to files coefficient matrices Gm, Zm.

      SUBROUTINE build_coeff_mtrx(imin, imax, jmin, jmax, kmin, kmax, &
                   enc, ord, G, Z, Gm, Zm, fhg, fhz, fnameg, fnamez)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax
      integer, intent(in) :: enc(imin:,jmin:,kmin:), ord, fhg, fhz
      real(p), intent(in) :: G(1:,1:), Z(1:, 1:)

      character(len=*), intent(in) :: fnameg, fnamez

!     Coefficient matrix
      real(p), intent(inout) :: Gm(imin:,jmin:,kmin:,1:)
      real(p), intent(inout) :: Zm(imin:,jmin:,kmin:,1:)

!     Point coordinates
      integer :: i, j, k


      if (verbose_flag) then

        print *, "Building coefficient matrices..."

      end if

      select case (ord)

        case(2)

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax
    
                Gm(i,j,k,1) =  G(enc(i,j,k),1)
                Gm(i,j,k,2) =  G(enc(i,j,k),2)
                Gm(i,j,k,3) = -Gm(i,j,k,2)
          
                Zm(i,j,k,1) =  Z(enc(i,j,k),1)
                Zm(i,j,k,2) =  Z(enc(i,j,k),2)
                Zm(i,j,k,3) = -Zm(i,j,k,2)
    
              end do
            end do
          end do
          !$omp end parallel do

        case(4)

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax
    
                Gm(i,j,k,1) =                 G(enc(i,j,k),1)
                Gm(i,j,k,2) = 27.0d+0/24.0d+0*G(enc(i,j,k),2)
                Gm(i,j,k,3) =                -G(enc(i,j,k),2)/24.0d+0
                Gm(i,j,k,4) =                -Gm(i,j,k,2)
                Gm(i,j,k,5) =                -Gm(i,j,k,3)
          
                Zm(i,j,k,1) =                 Z(enc(i,j,k),1)
                Zm(i,j,k,2) = 27.0d+0/24.0d+0*Z(enc(i,j,k),2)
                Zm(i,j,k,3) =                -Z(enc(i,j,k),2)/24.0d+0
                Zm(i,j,k,4) =                -Zm(i,j,k,2)
                Zm(i,j,k,5) =                -Zm(i,j,k,3)
          
              end do
            end do
          end do
          !$omp end parallel do

        case default

          stop "Error: Incorrect order of spatial discretisation!"

      end select

!>    @todo Implement efficient output in binary format
!>    @test Output coefficient matrices Gm, Zm
      if (verbose_flag) then

        call open_out_file(fhg, fnameg, "(A)", "#  Gm1  |  G1  |  Gm2  |  G2")
        call open_out_file(fhz, fnamez, "(A)", "#  Zm1  |  Z1  |  Zm2  |  Z2")
  
        select case (ord)

          case(2)

            do k = kmin, kmax
              do j = jmin, jmax
                do i = imin, imax
        
                  write(fhg,*) Gm(i,j,k,1), G(enc(i,j,k),1), &
                               Gm(i,j,k,2), G(enc(i,j,k),2)
      
                  write(fhz,*) Zm(i,j,k,1), Z(enc(i,j,k),1), &
                               Zm(i,j,k,2), Z(enc(i,j,k),2)
                end do
              end do
            end do

          case(4)

            do k = kmin, kmax
              do j = jmin, jmax
                do i = imin, imax
        
                  write(fhg,*) Gm(i,j,k,1),             G(enc(i,j,k),1),    &
                               Gm(i,j,k,2),  27.0d+0/24*G(enc(i,j,k),2),    &
                               Gm(i,j,k,3),            -G(enc(i,j,k),2)/24, &
                               Gm(i,j,k,4), -27.0d+0/24*G(enc(i,j,k),2),    &
                               Gm(i,j,k,5),             G(enc(i,j,k),2)/24
      
                  write(fhz,*) Zm(i,j,k,1),             Z(enc(i,j,k),1),    &
                               Zm(i,j,k,2),  27.0d+0/24*Z(enc(i,j,k),2),    &
                               Zm(i,j,k,3),            -Z(enc(i,j,k),2)/24, &
                               Zm(i,j,k,4), -27.0d+0/24*Z(enc(i,j,k),2),    &
                               Zm(i,j,k,5),             Z(enc(i,j,k),2)/24
                end do
              end do
            end do

          case default

            stop "Error: Incorrect order of spatial discretisation"

        end select
  
        call close_out_file(fhg, fnameg)
        call close_out_file(fhz, fnamez)

      end if

      call dealloc_coeff_arrays

      END SUBROUTINE build_coeff_mtrx

!-----------------------------------------------------------------------

!>    Calculates field value at excitation point location
!!
!!    @param[in] E     electric field or flux density array at given location
!!    @param[in] G2    coefficient
!!    @param[in] J     electric current density at given time step
!!    @param[in] stype excitation source type

      real(p) FUNCTION point_src(E, G2, J, stype)

      IMPLICIT NONE

      real(p),          intent(in) :: E, G2, J
      character(len=4), intent(in) :: stype


      ! Deafult behaviour: leave field value intact
      point_src = E

      select case (stype)

        case ("hard")

          point_src = -G2*J

        case ("soft")

          point_src = E - G2*J

      end select

      END FUNCTION point_src

!-----------------------------------------------------------------------

!>    Calculates electric field or electric flux density values
!!    at excitation sources.
!!
!!    @note This subroutine can also be used to excite magnetic field
!!          or magnetic flux density.
!!
!!    @param[in] E          electric field array
!!    @param[in] comment    component to modify
!!    @param[in] {i,j,k}min spatial domain lower boundaries
!!    @param[in] dl         decrement for lower boundaries of spatial domain
!!    @param[in] nsrc       no. of sources
!!    @param[in] src        source locations array
!!    @param[in] stype      source type (hard/soft)
!!    @param[in] G          coefficients, G(nmats,2)
!!    @param[in] enc        material encoding locations array
!!    @param[in] J          electric current density
!!    @param[in] n0         start index of current density array
!!    @param[in] t          time step value

      SUBROUTINE calc_E_src(E, comment, imin, jmin, kmin, dl, nsrc, src, &
                            stype, G, enc, J, n0, t)

      integer, intent(in) :: imin, jmin, kmin, dl, nsrc, n0, t
      integer, intent(in) :: enc(imin:,jmin:,kmin:)

      real(p),          intent(inout) :: E(imin-dl:,jmin-dl:,kmin-dl:)
      type(point),      intent(in)    :: src(nsrc)
      character(len=*), intent(in)    :: comment
      character(len=4), intent(in)    :: stype

      real(p), intent(in) :: G(1:,1:), J(n0:)

!     Point source counter
      integer :: s

!     Source point
      type(point) :: pt


      if (verbose_flag) then

        msg = "Calculating " // comment // &
              " value at excitation source location..."

        print *, msg

      end if

!     Update values at each excitation point location
      do s = 1, nsrc

        pt = src(s)

        E(pt%x,pt%y,pt%z) = point_src(E(pt%x,pt%y,pt%z), &
                              G(enc(pt%x,pt%y,pt%z),2), J(t), stype)
      end do

!     Print out verification information
      write (vh,*) t, J(t)

      END SUBROUTINE calc_E_src

!-----------------------------------------------------------------------

!>    Calculates electric field or electric flux density values
!!    at excitation sources.
!!
!!    @note This subroutine can also be used to excite magnetic field
!!          or magnetic flux density.
!!
!!    @param[in] E          electric field array
!!    @param[in] comment    component to modify
!!    @param[in] {i,j,k}min spatial domain lower boundaries
!!    @param[in] dl         decrement for lower boundaries of spatial domain
!!    @param[in] nsrc       no. of sources
!!    @param[in] src        source locations array
!!    @param[in] stype      source type (hard/soft)
!!    @param[in] Gm         pre-computed coefficients in matrix form
!!    @param[in] J          electric current density
!!    @param[in] n0         start index of current density array
!!    @param[in] t          time step value

      SUBROUTINE calc_E_src_pre(E, comment, imin, jmin, kmin, dl, nsrc, src, &
                                stype, Gm, J, n0, t)

      integer, intent(in) :: imin, jmin, kmin, dl, nsrc, n0, t

      real(p),          intent(inout) :: E(imin-dl:,jmin-dl:,kmin-dl:)
      type(point),      intent(in)    :: src(nsrc)
      character(len=*), intent(in)    :: comment
      character(len=4), intent(in)    :: stype

      real(p), intent(in) :: Gm(imin:,jmin:,kmin:,1:), J(n0:)

!     Point source counter
      integer :: s

!     Source point
      type(point) :: pt


      if (verbose_flag) then

        msg = "Calculating " // comment // &
              " value at excitation source location..."

        print *, msg

      end if

!     Update values at each excitation point location
      do s = 1, nsrc

        pt = src(s)

        E(pt%x,pt%y,pt%z) = point_src(E(pt%x,pt%y,pt%z),       &
                              Gm(pt%x,pt%y,pt%z,2), J(t), stype)
      end do

!     Print out verification information
      write (vh,*) t, J(t)

      END SUBROUTINE calc_E_src_pre

!-----------------------------------------------------------------------

!>    Calculates electric field values at current time step (n)
!!    for entire spatial domain. Wrapper subroutine.
!!
!!    @param[in,out] E               electric field components x, y, z
!!    @param[in]     H               magnetic field components x, y, z
!!    @param[in]     eqtype          equation type (expl, func, dotp, matm)
!!    @param[in]     G               coefficients array, G(nmats,2)
!!    @param[in]     enc             material encoding locations array
!!    @param[in]    {i,j,k}{min,max} simulation domain boundaries
!!    @param[in]     dx, dy, dz      spatial increments

      SUBROUTINE calc_E_star(E, H, eqtype, G, enc, imin, imax, &
                             jmin, jmax, kmin, kmax, dx, dy, dz)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax
      integer, intent(in) :: enc(imin:,jmin:,kmin:)

      real(p), intent(inout) :: E(imin:,  jmin:,  kmin:,  1:)
      real(p), intent(in)    :: H(imin-1:,jmin-1:,kmin-1:,1:)

      real(p),   intent(in) :: G(1:,1:), dx, dy, dz

      character(len=4), intent(in) :: eqtype

!     Point coordinates
      integer :: i = 0, j = 0, k = 0


      if (verbose_flag) then

        print *, "Calculating electric field values..."

      end if

!     Calculate E{x,y,z}

      select case (eqtype)

        case ("func")  ! func fly

          if (verbose_flag)  print *, "Eq. type: func"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          E(i,j,k,1) = calc_E(G(enc(i,j,k),:), E(i,j,k,1),  &
                              H(i,j,k,3), H(i,j-1,k,3), dy, &
                              H(i,j,k,2), H(i,j,k-1,2), dz)
    
          E(i,j,k,2) = calc_E(G(enc(i,j,k),:), E(i,j,k,2),  &
                              H(i,j,k,1), H(i,j,k-1,1), dz, &
                              H(i,j,k,3), H(i-1,j,k,3), dx)
    
          E(i,j,k,3) = calc_E(G(enc(i,j,k),:), E(i,j,k,3),  &
                              H(i,j,k,2), H(i-1,j,k,2), dx, &
                              H(i,j,k,1), H(i,j-1,k,1), dy)
              end do
            end do
          end do
          !$omp end parallel do

        case ("dotp")  ! dot prod fly

          if (verbose_flag)  print *, "Eq. type: dotp"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax
          
          E(i,j,k,1) = dot_product([G(enc(i,j,k),:), -G(enc(i,j,k),2)],       &
                                   [E(i,j,k,1), (H(i,j,k,3)-H(i,j-1,k,3))/dy, &
                                                (H(i,j,k,2)-H(i,j,k-1,2))/dz])
    
          E(i,j,k,2) = dot_product([G(enc(i,j,k),:), -G(enc(i,j,k),2)],       &
                                   [E(i,j,k,2), (H(i,j,k,1)-H(i,j,k-1,1))/dz, &
                                                (H(i,j,k,3)-H(i-1,j,k,3))/dx])
    
          E(i,j,k,3) = dot_product([G(enc(i,j,k),:), -G(enc(i,j,k),2)],       &
                                   [E(i,j,k,3), (H(i,j,k,2)-H(i-1,j,k,2))/dx, &
                                                (H(i,j,k,1)-H(i,j-1,k,1))/dy])
              end do
            end do
          end do
          !$omp end parallel do

        case ("matm")  ! matm fly

          if (verbose_flag)  print *, "Eq. type: matm"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax
          
          E(i,j,k,:) = matmul([G(enc(i,j,k),:), -G(enc(i,j,k),2)],                  &
                         reshape([E(i,j,k,1), (H(i,j,k,3)-H(i,j-1,k,3))/dy,         &
                                              (H(i,j,k,2)-H(i,j,k-1,2))/dz,         &
                                  E(i,j,k,2), (H(i,j,k,1)-H(i,j,k-1,1))/dz,         &
                                              (H(i,j,k,3)-H(i-1,j,k,3))/dx,         &
                                  E(i,j,k,3), (H(i,j,k,2)-H(i-1,j,k,2))/dx,         &
                                              (H(i,j,k,1)-H(i,j-1,k,1))/dy], [3, 3]))
              end do
            end do
          end do
          !$omp end parallel do

        case default  ! expl fly

          if (verbose_flag)  print *, "Eq. type: expl"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          E(i,j,k,1) = G(enc(i,j,k),1) *   E(i,j,k,1) +                  &
                       G(enc(i,j,k),2) * ((H(i,j,k,3)-H(i,j-1,k,3))/dy - &
                                          (H(i,j,k,2)-H(i,j,k-1,2))/dz)
    
          E(i,j,k,2) = G(enc(i,j,k),1) *   E(i,j,k,2) +                  &
                       G(enc(i,j,k),2) * ((H(i,j,k,1)-H(i,j,k-1,1))/dz - &
                                          (H(i,j,k,3)-H(i-1,j,k,3))/dx)
    
          E(i,j,k,3) = G(enc(i,j,k),1) *   E(i,j,k,3) +                  &
                       G(enc(i,j,k),2) * ((H(i,j,k,2)-H(i-1,j,k,2))/dx - &
                                          (H(i,j,k,1)-H(i,j-1,k,1))/dy)
              end do
            end do
          end do
          !$omp end parallel do

        end select

      END SUBROUTINE calc_E_star

!-----------------------------------------------------------------------

!>    Calculates electric field values at current time step (n)
!!    for entire spatial domain. Wrapper subroutine.
!!
!!    @note Uses pre-computed coefficidents Gamma in matrix form
!!
!!    @param[in,out] E               electric field components x, y, z
!!    @param[in]     H               magnetic field components x, y, z
!!    @param[in]     eqtype          equation type (expl,func,dotp,ddot,dgmv,matm,dgmm)
!!    @param[in]     Gm              coefficients in matrix form, Gm(i,j,k,2)
!!    @param[in]    {i,j,k}{min,max} simulation domain boundaries
!!    @param[in]     dx, dy, dz      spatial increments

      SUBROUTINE calc_E_star_pre(E, H, eqtype, Gm, imin, imax, &
                                 jmin, jmax, kmin, kmax, dx, dy, dz)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax

      real(p), intent(inout) :: E(imin:,  jmin:,  kmin:,  1:)
      real(p), intent(in)    :: H(imin-1:,jmin-1:,kmin-1:,1:)

      real(p),   intent(in) :: Gm(imin:,jmin:,kmin:,1:), dx, dy, dz

      character(len=4), intent(in) :: eqtype

!     Point coordinates
      integer :: i, j, k


      if (verbose_flag) then

        print *, "Calculating electric field values..."

      end if

!     Calculate E{x,y,z}

      select case (eqtype)

        case ("func")  ! func pre

          if (verbose_flag)  print *, "Eq. type: func"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          E(i,j,k,1) = calc_E(Gm(i,j,k,1:2), E(i,j,k,1),     &
                               H(i,j,k,3), H(i,j-1,k,3), dy, &
                               H(i,j,k,2), H(i,j,k-1,2), dz)
    
          E(i,j,k,2) = calc_E(Gm(i,j,k,1:2), E(i,j,k,2),     &
                               H(i,j,k,1), H(i,j,k-1,1), dz, &
                               H(i,j,k,3), H(i-1,j,k,3), dx)
    
          E(i,j,k,3) = calc_E(Gm(i,j,k,1:2), E(i,j,k,3),     &
                               H(i,j,k,2), H(i-1,j,k,2), dx, &
                               H(i,j,k,1), H(i,j-1,k,1), dy)
              end do
            end do
          end do
          !$omp end parallel do

        case ("dotp")  ! dot prod pre

          if (verbose_flag)  print *, "Eq. type: dotp"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax
          
          E(i,j,k,1) = dot_product(Gm(i,j,k,:), [E(i,j,k,1),     &
                                   (H(i,j,k,3)-H(i,j-1,k,3))/dy, &
                                   (H(i,j,k,2)-H(i,j,k-1,2))/dz])
    
          E(i,j,k,2) = dot_product(Gm(i,j,k,:), [E(i,j,k,2),     &
                                   (H(i,j,k,1)-H(i,j,k-1,1))/dz, &
                                   (H(i,j,k,3)-H(i-1,j,k,3))/dx])
    
          E(i,j,k,3) = dot_product(Gm(i,j,k,:), [E(i,j,k,3),     &
                                   (H(i,j,k,2)-H(i-1,j,k,2))/dx, &
                                   (H(i,j,k,1)-H(i,j-1,k,1))/dy])
              end do
            end do
          end do
          !$omp end parallel do

        case ("matm")  ! matm pre

          if (verbose_flag)  print *, "Eq. type: matm"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax
          
          E(i,j,k,:) = matmul(Gm(i,j,k,:),                                          &
                         reshape([E(i,j,k,1), (H(i,j,k,3)-H(i,j-1,k,3))/dy,         &
                                              (H(i,j,k,2)-H(i,j,k-1,2))/dz,         &
                                  E(i,j,k,2), (H(i,j,k,1)-H(i,j,k-1,1))/dz,         &
                                              (H(i,j,k,3)-H(i-1,j,k,3))/dx,         &
                                  E(i,j,k,3), (H(i,j,k,2)-H(i-1,j,k,2))/dx,         &
                                              (H(i,j,k,1)-H(i,j-1,k,1))/dy], [3, 3]))
              end do
            end do
          end do
          !$omp end parallel do

        case default  ! expl pre

          if (verbose_flag)  print *, "Eq. type: expl"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          E(i,j,k,1) = Gm(i,j,k,1) * E(i,j,k,1) +                    &
                       Gm(i,j,k,2) * ((H(i,j,k,3)-H(i,j-1,k,3))/dy - &
                                      (H(i,j,k,2)-H(i,j,k-1,2))/dz)
    
          E(i,j,k,2) = Gm(i,j,k,1) * E(i,j,k,2) +                    &
                       Gm(i,j,k,2) * ((H(i,j,k,1)-H(i,j,k-1,1))/dz - &
                                      (H(i,j,k,3)-H(i-1,j,k,3))/dx)
    
          E(i,j,k,3) = Gm(i,j,k,1) * E(i,j,k,3) +                    &
                       Gm(i,j,k,2) * ((H(i,j,k,2)-H(i-1,j,k,2))/dx - &
                                      (H(i,j,k,1)-H(i,j-1,k,1))/dy)
              end do
            end do
          end do
          !$omp end parallel do

        end select

      END SUBROUTINE calc_E_star_pre

!-----------------------------------------------------------------------

!>    Calculates magnetic field values at current time step (n)
!!    for entire spatial domain. Wrapper subroutine.
!!
!!    @param[in,out] H               magnetic field components x, y, z
!!    @param[in]     E               electric field components x, y, z
!!    @param[in]     eqtype          equation type (expl,func,dotp,ddot,dgmv,matm,dgmm)
!!    @param[in]     Z               coefficients array, Z(nmats,2)
!!    @param[in]     enc             material encoding locations array
!!    @param[in]    {i,j,k}{min,max} simulation domain boundaries
!!    @param[in]     dx, dy, dz      spatial increments

      SUBROUTINE calc_H_star(H, E, eqtype, Z, enc, imin, imax, &
                             jmin, jmax, kmin, kmax, dx, dy, dz)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax
      integer, intent(in) :: enc(imin:,jmin:,kmin:)

      real(p), intent(inout) :: H(imin-1:,jmin-1:,kmin-1:,1:)
      real(p), intent(in)    :: E(imin:,  jmin:,  kmin:,  1:)

      real(p),   intent(in) :: Z(1:,1:), dx, dy, dz

      character(len=4), intent(in) :: eqtype

!     Point coordinates
      integer :: i = 0, j = 0, k = 0


      if (verbose_flag) then

        print *, "Calculating magnetic field values..."

      end if

!     Calculate H{x,y,z}

      select case (eqtype)

        case ("func")  ! func fly

          if (verbose_flag)  print *, "Eq. type: func"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = calc_H(Z(enc(i,j,k),:), H(i,j,k,1),  &
                              E(i,j,k+1,2), E(i,j,k,2), dz, &
                              E(i,j+1,k,3), E(i,j,k,3), dy)
    
          H(i,j,k,2) = calc_H(Z(enc(i,j,k),:), H(i,j,k,2),  &
                              E(i+1,j,k,3), E(i,j,k,3), dx, &
                              E(i,j,k+1,1), E(i,j,k,1), dz)
    
          H(i,j,k,3) = calc_H(Z(enc(i,j,k),:), H(i,j,k,3),  &
                              E(i,j+1,k,1), E(i,j,k,1), dy, &
                              E(i+1,j,k,2), E(i,j,k,2), dx)
              end do
            end do
          end do
          !$omp end parallel do

        case ("dotp")  ! dot prod fly

          if (verbose_flag)  print *, "Eq. type: dotp"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = dot_product([Z(enc(i,j,k),:), -Z(enc(i,j,k),2)],       &
                                   [H(i,j,k,1), (E(i,j,k+1,2)-E(i,j,k,2))/dz, &
                                                (E(i,j+1,k,3)-E(i,j,k,3))/dy])
    
          H(i,j,k,2) = dot_product([Z(enc(i,j,k),:), -Z(enc(i,j,k),2)],       &
                                   [H(i,j,k,2), (E(i+1,j,k,3)-E(i,j,k,3))/dx, &
                                                (E(i,j,k+1,1)-E(i,j,k,1))/dz])
    
          H(i,j,k,3) = dot_product([Z(enc(i,j,k),:), -Z(enc(i,j,k),2)],       &
                                   [H(i,j,k,3), (E(i,j+1,k,1)-E(i,j,k,1))/dy, &
                                                (E(i+1,j,k,2)-E(i,j,k,2))/dx])
              end do
            end do
          end do
          !$omp end parallel do

        case ("matm")  ! matm fly

          if (verbose_flag)  print *, "Eq. type: matm"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,:) = matmul([Z(enc(i,j,k),:), -Z(enc(i,j,k),2)],                  &
                         reshape([H(i,j,k,1), (E(i,j,k+1,2)-E(i,j,k,2))/dz,         &
                                              (E(i,j+1,k,3)-E(i,j,k,3))/dy,         &
                                  H(i,j,k,2), (E(i+1,j,k,3)-E(i,j,k,3))/dx,         &
                                              (E(i,j,k+1,1)-E(i,j,k,1))/dz,         &
                                  H(i,j,k,3), (E(i,j+1,k,1)-E(i,j,k,1))/dy,         &
                                              (E(i+1,j,k,2)-E(i,j,k,2))/dx], [3, 3]))
              end do
            end do
          end do
          !$omp end parallel do

        case default  ! expl fly

          if (verbose_flag)  print *, "Eq. type: expl"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = Z(enc(i,j,k),1) *   H(i,j,k,1) +                  &
                       Z(enc(i,j,k),2) * ((E(i,j,k+1,2)-E(i,j,k,2))/dz - &
                                          (E(i,j+1,k,3)-E(i,j,k,3))/dy)
    
          H(i,j,k,2) = Z(enc(i,j,k),1) *   H(i,j,k,2) +                  &
                       Z(enc(i,j,k),2) * ((E(i+1,j,k,3)-E(i,j,k,3))/dx - &
                                          (E(i,j,k+1,1)-E(i,j,k,1))/dz)
    
          H(i,j,k,3) = Z(enc(i,j,k),1) *   H(i,j,k,3) +                  &
                       Z(enc(i,j,k),2) * ((E(i,j+1,k,1)-E(i,j,k,1))/dy - &
                                          (E(i+1,j,k,2)-E(i,j,k,2))/dx)
              end do
            end do
          end do
          !$omp end parallel do

      end select

      END SUBROUTINE calc_H_star

!-----------------------------------------------------------------------

!>    Calculates magnetic field values at current time step (n)
!!    for entire spatial domain. Wrapper subroutine.
!!
!!    @note Uses pre-computed coefficidents Zeta in matrix form
!!
!!    @param[in,out] H               magnetic field components x, y, z
!!    @param[in]     E               electric field components x, y, z
!!    @param[in]     eqtype          equation type (expl,func,dotp,ddot,dgmv,matm,dgmm)
!!    @param[in]     Zm              coefficients in matrix form, Zm(i,j,k,2)
!!    @param[in]    {i,j,k}{min,max} simulation domain boundaries
!!    @param[in]     dx, dy, dz      spatial increments

      SUBROUTINE calc_H_star_pre(H, E, eqtype, Zm, imin, imax,     &
                                 jmin, jmax, kmin, kmax, dx, dy, dz)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax

      real(p), intent(inout) :: H(imin-1:,jmin-1:,kmin-1:,1:)
      real(p), intent(in)    :: E(imin:,  jmin:,  kmin:,  1:)

      real(p),   intent(in) :: Zm(imin:,jmin:,kmin:,1:), dx, dy, dz

      character(len=4), intent(in) :: eqtype

!     Point coordinates
      integer :: i, j, k


      if (verbose_flag) then

        print *, "Calculating magnetic field values..."

      end if

!     Calculate H{x,y,z}

      select case (eqtype)

        case ("func")  ! func pre

          if (verbose_flag)  print *, "Eq. type: func"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = calc_H(Zm(i,j,k,1:2), H(i,j,k,1),     &
                               E(i,j,k+1,2), E(i,j,k,2), dz, &
                               E(i,j+1,k,3), E(i,j,k,3), dy)
    
          H(i,j,k,2) = calc_H(Zm(i,j,k,1:2), H(i,j,k,2),     &
                               E(i+1,j,k,3), E(i,j,k,3), dx, &
                               E(i,j,k+1,1), E(i,j,k,1), dz)
    
          H(i,j,k,3) = calc_H(Zm(i,j,k,1:2), H(i,j,k,3),     &
                               E(i,j+1,k,1), E(i,j,k,1), dy, &
                               E(i+1,j,k,2), E(i,j,k,2), dx)
              end do
            end do
          end do
          !$omp end parallel do

        case ("dotp")  ! dot prod pre

          if (verbose_flag)  print *, "Eq. type: dotp"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = dot_product(Zm(i,j,k,:), [H(i,j,k,1),     &
                                   (E(i,j,k+1,2)-E(i,j,k,2))/dz, &
                                   (E(i,j+1,k,3)-E(i,j,k,3))/dy])
    
          H(i,j,k,2) = dot_product(Zm(i,j,k,:), [H(i,j,k,2),     &
                                   (E(i+1,j,k,3)-E(i,j,k,3))/dx, &
                                   (E(i,j,k+1,1)-E(i,j,k,1))/dz])
    
          H(i,j,k,3) = dot_product(Zm(i,j,k,:), [H(i,j,k,3),     &
                                   (E(i,j+1,k,1)-E(i,j,k,1))/dy, &
                                   (E(i+1,j,k,2)-E(i,j,k,2))/dx])
              end do
            end do
          end do
          !$omp end parallel do

        case ("matm")  ! matm pre

          if (verbose_flag)  print *, "Eq. type: matm"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,:) = matmul(Zm(i,j,k,:),                                          &
                         reshape([H(i,j,k,1), (E(i,j,k+1,2)-E(i,j,k,2))/dz,         &
                                              (E(i,j+1,k,3)-E(i,j,k,3))/dy,         &
                                  H(i,j,k,2), (E(i+1,j,k,3)-E(i,j,k,3))/dx,         &
                                              (E(i,j,k+1,1)-E(i,j,k,1))/dz,         &
                                  H(i,j,k,3), (E(i,j+1,k,1)-E(i,j,k,1))/dy,         &
                                              (E(i+1,j,k,2)-E(i,j,k,2))/dx], [3, 3]))
              end do
            end do
          end do
          !$omp end parallel do

        case default  ! expl pre

          if (verbose_flag)  print *, "Eq. type: expl"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = Zm(i,j,k,1) *   H(i,j,k,1) +                  &
                       Zm(i,j,k,2) * ((E(i,j,k+1,2)-E(i,j,k,2))/dz - &
                                      (E(i,j+1,k,3)-E(i,j,k,3))/dy)
    
          H(i,j,k,2) = Zm(i,j,k,1) *   H(i,j,k,2) +                  &
                       Zm(i,j,k,2) * ((E(i+1,j,k,3)-E(i,j,k,3))/dx - &
                                      (E(i,j,k+1,1)-E(i,j,k,1))/dz)
    
          H(i,j,k,3) = Zm(i,j,k,1) *   H(i,j,k,3) +                  &
                       Zm(i,j,k,2) * ((E(i,j+1,k,1)-E(i,j,k,1))/dy - &
                                      (E(i+1,j,k,2)-E(i,j,k,2))/dx)
              end do
            end do
          end do
          !$omp end parallel do

      end select

      END SUBROUTINE calc_H_star_pre

!-----------------------------------------------------------------------

!>    Calculates electric field value E at time step (n)
!!    for given spatial location (i,j)
!!
!!    @param[in] G          coefficients
!!    @param[in] E          electric field at time step (n-1)
!!    @param[in] H{1,2,3,4} magnetic field at time step (n)
!!    @param[in] ds{1,2}    spatial increment over x,y axes

      real(p) FUNCTION calc_E(G, E, H1, H2, ds1, H3, H4, ds2)

      IMPLICIT NONE

      real(p), intent(in) :: G(2), E, H1, H2, ds1, H3, H4, ds2

      calc_E = G(1)*E + G(2)*( (H1-H2)/ds1 - (H3-H4)/ds2 )

      END FUNCTION calc_E

!-----------------------------------------------------------------------

!>    Calculates magnetic field value H at time step (n+1)
!!    for given spatial location (i,j)
!!
!!    @param[in] Z          coefficients
!!    @param[in] H          magnetic field at time step (n)
!!    @param[in] E{1,2,3,4} electric field at time step (n)
!!    @param[in] ds{1,2}    spatial increments over x,y axes

      real(p) FUNCTION calc_H(Z, H, E1, E2, ds1, E3, E4, ds2)

      IMPLICIT NONE

      real(p), intent(in) :: Z(2), H, E1, E2, ds1, E3, E4, ds2

      calc_H = Z(1)*H + Z(2)*( (E1-E2)/ds1 - (E3-E4)/ds2 )

      END FUNCTION calc_H

!-----------------------------------------------------------------------

!>    Calculates temporal increment based on Courant-Friedrichs-Lewy
!!    condition

      real(p) FUNCTION calc_dt(ncfl, C0, dx, dy, dz, ord, dim)

      IMPLICIT NONE

      real(p), intent(inout) :: ncfl
      real(p), intent(in)    :: C0, dx, dy, dz
      integer, intent(in)    :: ord, dim


      ! Fang's (2,4) scheme
      if (ord == 4)  ncfl = 6.0d+0/7.0d+0

      select case (dim)

        case (1)
          calc_dt = ncfl/C0*dx

        case (2)
          calc_dt = ncfl/C0/dsqrt(dx**(-2.0d+0)+dy**(-2.0d+0))

        case (3)
          calc_dt = ncfl/C0/dsqrt(dx**(-2.0d+0)+dy**(-2.0d+0)+dz**(-2.0d+0))

        case default
          stop "Error: Dimensionality 'dim' is incorrect!"

      end select

      END FUNCTION calc_dt

!-----------------------------------------------------------------------

      END MODULE YEE

!-----------------------------------------------------------------------
!!    @eof yee.f90
!-----------------------------------------------------------------------
