!-----------------------------------------------------------------------
!
!>    @brief Implements Gaussian pulse excitation source for 3D code
!!
!!    @author  Maksims Abalenkovs
!!    @email   maksims.abalenkovs@stfc.ac.uk
!!    @date    Oct 31, 2023
!!    @version 0.7
!
!-----------------------------------------------------------------------

      MODULE GAUSSIAN

      USE IO
      USE VARIABLES

      IMPLICIT NONE

!-----------------------------------------------------------------------

      CONTAINS

!-----------------------------------------------------------------------

!>    Generates Gaussian pulse excitation source as electric current
!!    density J. Gaussian pulse is unmodulated, specified by following
!!    formula:
!!
!!      \f[ f(t) = \exp( -((t-3T)/T)^2 ), \f]
!!
!!    where \f$ T = 1/(2 f_{max}) \f$ and \f$ t = \Delta t \cdot t_i. \f$
!!
!!    Due to inability to store actual \f$ f(t+1/2) \f$ value in Fortran,
!!    new value \f$ f(t+1/2) \f$ is enforced onto \f$ f(t). \f$
!!
!!    To achieve current density amplitudes comparable to 1.0 division
!!    by \f$ \Delta t \f$ is omitted:
!!
!!      \f[ f(t) = f(t+1)-f(t). \f]
!!
!!    @param[in] J    electric current density array
!!    @param[in] dt   temporal increment
!!    @param[in] n0   start index of electric current density array
!!    @param[in] fmax maximum frequency of interest

      SUBROUTINE generate_gaussian(J, dt, n0, fmax)

      integer, intent(in)    :: n0
      real(p), intent(inout) :: J(n0:)
      real(p), intent(in)    :: dt
      real(p), intent(in)    :: fmax

      real(p) :: machPrec
      integer :: nmax

!     Time step counter
      integer :: n = 0

!     Output file name
      character(len=11), parameter :: fname = "gpulse.txt"

!     Output file handle (unit number)
      integer, parameter :: fh = 21


!     Set machine precision
      machPrec = epsilon(J(n0))

!     Set maximum number of timesteps for which Gaussian pulse is
!     greater than machine precision 'machPrec'
      nmax = floor(abs((3.0-sqrt(cmplx(log(machPrec),kind=p)))/2.0/fmax/dt))

      ! @note Implicit loop does not work with Intel MKL BLAS ddot, ver. 15.0.3
      ! J = [ (exp(-(dt*real(n)*2.0*fmax-3.0)**2), n = n0, nmax) ]

      do n = n0, nmax
        J(n) = exp(-(dt*real(n)*2.0*fmax-3.0)**2)
      end do

!     @test: Save generated Gaussian pulse to file
      if (verbose_flag) then

        print *, "Opening verification file..."

      end if
  
      call open_out_file(fh, fname, "(A)", "#  n  |  Jn")

      do n = n0, nmax

        write(fh, *) n, J(n)

      end do

      if (verbose_flag) then

        print *, "Closing verification file..."

      end if
  
      call close_out_file(fh, fname)

      print "(A, ES10.3)", "machPrec: ", machPrec
      print "(A, I10.3)",  "nmax: ",     nmax

      END SUBROUTINE generate_gaussian

!-----------------------------------------------------------------------

      END MODULE GAUSSIAN

!-----------------------------------------------------------------------
!!    @eof gaussian.f90
!-----------------------------------------------------------------------
