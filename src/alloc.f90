!-----------------------------------------------------------------------
!
!>    Implements prototype of stencil calculation in 3D domain
!!    over given number of time iterations.
!!
!!    @author  Maksims Abalenkovs
!!    @email   m.abalenkovs@manchester.ac.uk
!!    @date    Apr 3, 2017
!!    @version 0.4
!!
!!    @brief Contains subroutine declarations for array allocation
!!
!!    Subroutines (2):
!!
!!      check_alloc_act()
!!      dealloc_coeff_arrays()
!
!-----------------------------------------------------------------------

      MODULE ALLOC

      USE CONST
      USE VARIABLES

      IMPLICIT NONE

!-----------------------------------------------------------------------

      CONTAINS

!-----------------------------------------------------------------------

!>    Checks correct result of (de)allocation action. Prints error
!!    message and code on unsuccessful (de)allocation action.
!!
!!    @param[in] act     action
!!    @param[in] arrname array name
!!    @param[in] err     error code

      SUBROUTINE check_alloc_act(act, arrname, err)

      character(len=*), intent(in) :: act
      character(len=*), intent(in) :: arrname
      integer,          intent(in) :: err

      if (err /= 0) then

        print *,     "Error: unable to " // act // " space for " // &
                      arrname // " data arrays"
        stop

      end if

      END SUBROUTINE check_alloc_act

!-----------------------------------------------------------------------

!>    Deallocates FDTD coefficient arrays enc, gamma, zeta

      SUBROUTINE dealloc_coeff_arrays

      if (verbose_flag) then

        print *, "Deallocating material encoding locations array..."

      end if

      deallocate( enc, stat=err )

      call check_alloc_act("deallocate",                     &
                           "material encoding locations", err)

      if (verbose_flag) then

        print *, "Deallocating material coefficient arrays..."

      end if

      deallocate( G, Z, stat=err )

      call check_alloc_act("deallocate", "material coefficient", err)

      END SUBROUTINE dealloc_coeff_arrays

!-----------------------------------------------------------------------

      END MODULE ALLOC

!-----------------------------------------------------------------------
