!-----------------------------------------------------------------------
!
!     Implements prototype of stencil calculation in 3D domain
!     over given number of time iterations.
! 
!>    @author  Maksims Abalenkovs
!!    @email   m.abalenkovs@manchester.ac.uk
!!    @date    Sep 12, 2017
!!    @version 0.11
!!
!!    @brief Contains subroutines for Mur,
!!           1st order Absorbing Boundary Conditions
!!           using Fang's (2,4) discretisation scheme
!!
!!    Function:
!!
!!      MurF_pt()
!!
!!    Subroutines (8):
!!
!!      calc_Mur_ABC_E4()
!!      calc_Mur_ABC_H4()
!!      calc_MurF_pt()
!!      calc_MurF_ABC()
!!      calc_MurF_ABC_H4()
!!      calc_MurF_ABC_E4()
!!      re_arrange_E4()
!!      re_arrange_H4()
!
!-----------------------------------------------------------------------

      MODULE MUR_FANG

      USE CONST
      USE MUR
      USE VARIABLES

      IMPLICIT NONE

      CONTAINS

!-----------------------------------------------------------------------

!>    Calculates electric field values at spatial domain
!!    boundaries. Wrapper subroutine making calls to 'calc_Mur_pt' to
!!    update magnetic field values at {i,j,k}{min,max} points.
!!
!!    @note: Stacked Mur's ABC version to be applied to Fang's (2,4)
!!           discretisation scheme.
!!
!!    @note: Can be used to calculate ABC for electric field, if
!!           appropriate corrections are made for spatial domain
!!           range indices.
!!
!!    Method: Mur, 1st order Absorbing Boundary Conditions
!!
!!    @param[in,out] E               electric field values at time step (n)
!!    @param[in]     Eu1n_v{min,max} electric field values at E(n-1)
!!    @param[in]    {i,j,k}{min,max} spatial domain range indices
!!    @param[in]     M{x,y,z}        coefficient for calculation of 1st order Mur ABC

      SUBROUTINE calc_Mur_ABC_E4(E, Ex1n_ymin, Ex1n_ymax, Ex1n_zmin, Ex1n_zmax, &
                                    Ey1n_xmin, Ey1n_xmax, Ey1n_zmin, Ey1n_zmax, &
                                    Ez1n_xmin, Ez1n_xmax, Ez1n_ymin, Ez1n_ymax, &
                                 imin, imax, jmin, jmax, kmin, kmax, Mx, My, Mz)
      IMPLICIT NONE

      integer, intent(in)    :: imin, imax, jmin, jmax, kmin, kmax

      real(p), intent(inout) :: E(imin-1:,jmin-1:,kmin-1:,1:)

      real(p), intent(in)    :: Ex1n_ymin(imin-1:,jmin-1:,kmin-1:), &
                                Ex1n_ymax(imin-1:,jmax:,  kmin-1:), &
                                Ex1n_zmin(imin-1:,jmin-1:,kmin-1:), &
                                Ex1n_zmax(imin-1:,jmin-1:,kmax:)

      real(p), intent(in)    :: Ey1n_xmin(imin-1:,jmin-1:,kmin-1:), &
                                Ey1n_xmax(imax:,  jmin-1:,kmin-1:), &
                                Ey1n_zmin(imin-1:,jmin-1:,kmin-1:), &
                                Ey1n_zmax(imin-1:,jmin-1:,kmax:)

      real(p), intent(in)    :: Ez1n_xmin(imin-1:,jmin-1:,kmin-1:), &
                                Ez1n_xmax(imax:,  jmin-1:,kmin-1:), &
                                Ez1n_ymin(imin-1:,jmin-1:,kmin-1:), &
                                Ez1n_ymax(imin-1:,jmax:,  kmin-1:)

      real(p),   intent(in) :: Mx, My, Mz

!     Point coordinates
      integer :: i, j, k


      if (verbose_flag)  print *, "Calculating Mur ABC..."

      !$omp parallel do
      do k = kmin-1, kmax+2
        do i = imin-1, imax+2

!     Calculate Ex, j{min,max}
!     Left  side (jmin)
      call calc_Mur_pt(E(i,jmin-1:jmin,  k,1), Ex1n_ymin(i,jmin-1:jmin,k),   jmin-1, jmin-1, +1, Mx)

!     Right side (jmax)
      call calc_Mur_pt(E(i,jmax  :jmax+1,k,1), Ex1n_ymax(i,jmax:  jmax+1,k), jmax,   jmax+1, -1, Mx)
      call calc_Mur_pt(E(i,jmax+1:jmax+2,k,1), Ex1n_ymax(i,jmax+1:jmax+2,k), jmax+1, jmax+2, -1, Mx)


!     Calculate Ez, j{min,max}
!     Left  side (jmin)
      call calc_Mur_pt(E(i,jmin-1:jmin,  k,3), Ez1n_ymin(i,jmin-1:jmin,k),   jmin-1, jmin-1, +1, Mz)

!     Right side (jmax)
      call calc_Mur_pt(E(i,jmax  :jmax+1,k,3), Ez1n_ymax(i,jmax:  jmax+1,k), jmax,   jmax+1, -1, Mz)
      call calc_Mur_pt(E(i,jmax+1:jmax+2,k,3), Ez1n_ymax(i,jmax+1:jmax+2,k), jmax+1, jmax+2, -1, Mz)

        end do
      end do
      !$omp end parallel do


      !$omp parallel do
      do j = jmin-1, jmax+2
        do i = imin-1, imax+2

!     Calculate Ex, k{min,max}
!     Left  side (kmin)
      call calc_Mur_pt(E(i,j,kmin-1:kmin,  1), Ex1n_zmin(i,j,kmin-1:kmin),   kmin-1, kmin-1, +1, Mx)

!     Right side (kmax)
      call calc_Mur_pt(E(i,j,kmax  :kmax+1,1), Ex1n_zmax(i,j,kmax  :kmax+1), kmax,   kmax+1, -1, Mx)
      call calc_Mur_pt(E(i,j,kmax+1:kmax+2,1), Ex1n_zmax(i,j,kmax+1:kmax+2), kmax+1, kmax+2, -1, Mx)


!     Calculate Ey, k{min,max}
!     Left  side (kmin)
      call calc_Mur_pt(E(i,j,kmin-1:kmin,  2), Ey1n_zmin(i,j,kmin-1:kmin),   kmin-1, kmin-1, +1, Mz)

!     Right side (kmax)
      call calc_Mur_pt(E(i,j,kmax  :kmax+1,2), Ey1n_zmax(i,j,kmax  :kmax+1), kmax,   kmax+1, -1, Mz)
      call calc_Mur_pt(E(i,j,kmax+1:kmax+2,2), Ey1n_zmax(i,j,kmax+1:kmax+2), kmax+1, kmax+2, -1, Mz)

        end do
      end do
      !$omp end parallel do


      !$omp parallel do
      do k = kmin-1, kmax+2
        do j = jmin-1, jmax+2

!     Calculate Ey, i{min,max}
!     Left  side (imin)
      call calc_Mur_pt(E(imin-1:imin,  j,k,2), Ey1n_xmin(imin-1:imin,  j,k), imin-1, imin-1, +1, My)

!     Right side (imax)
      call calc_Mur_pt(E(imax  :imax+1,j,k,2), Ey1n_xmax(imax  :imax+1,j,k), imax,   imax+1, -1, My)
      call calc_Mur_pt(E(imax+1:imax+2,j,k,2), Ey1n_xmax(imax+1:imax+2,j,k), imax+1, imax+2, -1, My)


!     Calculate Ez, i{min,max}
!     Left  side (imin)
      call calc_Mur_pt(E(imin-1:imin,  j,k,3), Ez1n_xmin(imin-1:imin,  j,k), imin-1, imin-1, +1, Mz)

!     Right side (imax)
      call calc_Mur_pt(E(imax  :imax+1,j,k,3), Ez1n_xmax(imax  :imax+1,j,k), imax,   imax+1, -1, Mz)
      call calc_Mur_pt(E(imax+1:imax+2,j,k,3), Ez1n_xmax(imax+1:imax+2,j,k), imax+1, imax+2, -1, Mz)

        end do
      end do
      !$omp end parallel do

      END SUBROUTINE calc_Mur_ABC_E4

!-----------------------------------------------------------------------

!>    Calculates magnetic field values at spatial domain
!!    boundaries. Wrapper subroutine making calls to 'calc_Mur_pt' to
!!    update magnetic field values at {i,j,k}{min,max} points.
!!
!!    @note: Stacked Mur's ABC version to be applied to Fang's (2,4)
!!           discretisation scheme.
!!
!!    @note: Can be used to calculate ABC for electric field, if
!!           appropriate corrections are made for spatial domain
!!           range indices.
!!
!!    Method: Mur, 1st order Absorbing Boundary Conditions
!!
!!    @param[in,out] H               magnetic field values at time step (n)
!!    @param[in]     Hu1n_v{min,max} magnetic field values at E(n-1)
!!    @param[in]    {i,j,k}{min,max} spatial domain range indices
!!    @param[in]     M{x,y,z}        coefficients for calculation of 1st order Mur ABC

      SUBROUTINE calc_Mur_ABC_H4(H, Hx1n_ymin, Hx1n_ymax, Hx1n_zmin, Hx1n_zmax, &
                                    Hy1n_xmin, Hy1n_xmax, Hy1n_zmin, Hy1n_zmax, &
                                    Hz1n_xmin, Hz1n_xmax, Hz1n_ymin, Hz1n_ymax, &
                                 imin, imax, jmin, jmax, kmin, kmax, Mx, My, Mz)

      integer, intent(in)    :: imin, imax, jmin, jmax, kmin, kmax

      real(p), intent(inout) :: H(imin-2:,jmin-2:,kmin-2:,1:)

      real(p), intent(in)    :: Hx1n_ymin(imin-2:,jmin-2:,kmin-2:), &
                                Hx1n_ymax(imin-2:,jmax:  ,kmin-2:), &
                                Hx1n_zmin(imin-2:,jmin-2:,kmin-2:), &
                                Hx1n_zmax(imin-2:,jmin-2:,kmax:)

      real(p), intent(in)    :: Hy1n_xmin(imin-2:,jmin-2:,kmin-2:), &
                                Hy1n_xmax(imax:  ,jmin-2:,kmin-2:), &
                                Hy1n_zmin(imin-2:,jmin-2:,kmin-2:), &
                                Hy1n_zmax(imin-2:,jmin-2:,kmax:)

      real(p), intent(in)    :: Hz1n_xmin(imin-2:,jmin-2:,kmin-2:), &
                                Hz1n_xmax(imax:  ,jmin-2:,kmin-2:), &
                                Hz1n_ymin(imin-2:,jmin-2:,kmin-2:), &
                                Hz1n_ymax(imin-2:,jmax:  ,kmin-2:)

      real(p), intent(in)    :: Mx, My, Mz

!     Point coordinates
      integer :: i, j, k


      if (verbose_flag)  print *, "Calculating Mur ABC (H4)..."

      !$omp parallel do
      do k = kmin-2, kmax+1
        do j = jmin-2, jmax+1

!     Calculate Hy, x{min,max}
!     Left  side (xmin)
      call calc_Mur_pt(H(imin-1:imin,  j,k,2), Hy1n_xmin(imin-1:imin,  j,k), imin-1, imin-1, +1, Mx)
      call calc_Mur_pt(H(imin-2:imin-1,j,k,2), Hy1n_xmin(imin-2:imin-1,j,k), imin-2, imin-2, +1, Mx)

!     Right side (xmax)
      call calc_Mur_pt(H(imax  :imax+1,j,k,2), Hy1n_xmax(imax  :imax+1,j,k), imax,   imax+1, -1, Mx)


!     Calculate Hz, x{min,max}
!     Left  side (xmin)
      call calc_Mur_pt(H(imin-1:imin,  j,k,3), Hz1n_xmin(imin-1:imin,  j,k), imin-1, imin-1, +1, Mx)
      call calc_Mur_pt(H(imin-2:imin-1,j,k,3), Hz1n_xmin(imin-2:imin-1,j,k), imin-2, imin-2, +1, Mx)

!     Right side (xmax)
      call calc_Mur_pt(H(imax  :imax+1,j,k,3), Hz1n_xmax(imax  :imax+1,j,k), imax,   imax+1, -1, Mx)

        end do
      end do
      !$omp end parallel do


      !$omp parallel do
      do k = kmin-2, kmax+1
        do i = imin-2, imax+1

!     Calculate Hx, y{min,max}
!     Left  side (jmin)
      call calc_Mur_pt(H(i,jmin-1:jmin,  k,1), Hx1n_ymin(i,jmin-1:jmin,  k), jmin-1, jmin-1, +1, My)
      call calc_Mur_pt(H(i,jmin-2:jmin-1,k,1), Hx1n_ymin(i,jmin-2:jmin-1,k), jmin-2, jmin-2, +1, My)

!     Right side (jmax)
      call calc_Mur_pt(H(i,jmax  :jmax+1,k,1), Hx1n_ymax(i,jmax  :jmax+1,k), jmax,   jmax+1, -1, My)


!     Calculate Hz, y{min,max}
!     Left  side (jmin)
      call calc_Mur_pt(H(i,jmin-1:jmin,  k,3), Hz1n_ymin(i,jmin-1:jmin,  k), jmin-1, jmin-1, +1, My)
      call calc_Mur_pt(H(i,jmin-2:jmin-1,k,3), Hz1n_ymin(i,jmin-2:jmin-1,k), jmin-2, jmin-2, +1, My)

!     Right side (jmax)
      call calc_Mur_pt(H(i,jmax  :jmax+1,k,3), Hz1n_ymax(i,jmax  :jmax+1,k), jmax,   jmax+1, -1, My)

        end do
      end do
      !$omp end parallel do


      !$omp parallel do
      do j = jmin-2, jmax+1
        do i = imin-2, imax+1

!     Calculate Hx, z{min,max}
!     Left  side (kmin)
      call calc_Mur_pt(H(i,j,kmin-1:kmin,  1), Hx1n_zmin(i,j,kmin-1:kmin),   kmin-1, kmin-1, +1, Mz)
      call calc_Mur_pt(H(i,j,kmin-2:kmin-1,1), Hx1n_zmin(i,j,kmin-2:kmin-1), kmin-2, kmin-2, +1, Mz)

!     Right side (kmax)
      call calc_Mur_pt(H(i,j,kmax  :kmax+1,1), Hx1n_zmax(i,j,kmax  :kmax+1), kmax,   kmax+1, -1, Mz)


!     Calculate Hy, z{min,max}
!     Left  side (kmin)
      call calc_Mur_pt(H(i,j,kmin-1:kmin,  2), Hy1n_zmin(i,j,kmin-1:kmin),   kmin-1, kmin-1, +1, Mz)
      call calc_Mur_pt(H(i,j,kmin-2:kmin-1,2), Hy1n_zmin(i,j,kmin-2:kmin-1), kmin-2, kmin-2, +1, Mz)

!     Right side (kmax)
      call calc_Mur_pt(H(i,j,kmax  :kmax+1,2), Hy1n_zmax(i,j,kmax  :kmax+1), kmax,   kmax+1, -1, Mz)

        end do
      end do
      !$omp end parallel do

      END SUBROUTINE calc_Mur_ABC_H4

!-----------------------------------------------------------------------

!>    Calculates electric field value for given location at spatial
!!    domain boundary.
!!    Method: Mur, 1st order Absorbing Boundary Conditions
!!    Scheme: Fang's (2,4) discretisation
!!
!!    @param[in] E{u}n{v} electric field values at time steps u in n, n-1
!!               and spatial locations v in i, i+/-1, i+/-2, i+/-3
!!    @param[in] M coefficients for calculation of 1st order Mur ABC

      real(p) FUNCTION MurF_pt(E1ni, E1ni1, E1ni2, E1ni3, Eni1, Eni2, Eni3, M)

      real(p), intent(in) :: E1ni, E1ni1, E1ni2, E1ni3, Eni1, Eni2, Eni3, M(2)

      MurF_pt = M(1)*(E1ni2 + Eni1) + M(2)*(Eni2 + E1ni1) - &
                E1ni3 - Eni3 - E1ni

      END FUNCTION MurF_pt

!-----------------------------------------------------------------------

!>    Calculates electric field value for given location at spatial 
!!    domain boundary. Wrapper subroutine necessary to make universal 
!!    calls at x{min,max} points.
!!
!!    Method: Mur, 1st order Absorbing Boundary Conditions
!!    Scheme: Fang's (2,4) discretisation
!!
!!    @param[in] En   electric field values at E(n)
!!    @param[in] E1n  electric field values at E(n-1)
!!    @param[in] x0   begin index of electromagnetic field arrays
!!    @param[in] pt   given boundary point x{min,max}
!!    @param[in] f    offset {+,-}1 for neighbouring point to x{min,max}
!!    @param[in] M    coefficients for calculation of 1st order Mur ABC

      SUBROUTINE calc_MurF_pt(En, E1n, x0, pt, f, M)

      integer, intent(in)    :: x0, pt, f
      real(p), intent(inout) :: En(x0:)
      real(p), intent(in)    :: E1n(x0:)
      real(p), intent(in)    :: M(2)

      En(pt) = MurF_pt(E1n(pt), E1n(pt+f), E1n(pt+2*f), E1n(pt+3*f),  &
                                 En(pt+f),  En(pt+2*f),  En(pt+3*f), M)

      END SUBROUTINE calc_MurF_pt

!-----------------------------------------------------------------------

!>    Calculates electric field values at spatial domain boundaries.
!!    Wrapper subroutine making calls to 'calc_MurF_pt' to update
!!    electric field values at x{min,max} points.
!!
!!    @note Can be used to calculate ABC for magnetic field if
!!    appropriate corrections are made for spatial domain range indices.
!!
!!    Method: Mur, 1st order Absorbing Boundary Conditions
!!    Scheme: Fang's (2,4) discretisation
!!
!!    @param[in] Ey              electric field values at time step (n)
!!    @param[in] Ey1n_x{min,max} electric field values at E(n-1)
!!    @param[in] imin, imax      spatial domain range indices
!!    @param[in] M

      SUBROUTINE calc_MurF_ABC(Ey, Ey1n_xmin, Ey1n_xmax, &
                               imin, imax, M)

      integer, intent(in)    :: imin, imax
      real(p), intent(inout) :: Ey(imin-1:)
      real(p), intent(in)    :: Ey1n_xmin(imin-1:), Ey1n_xmax(imax-2:)
      real(p), intent(in)    :: M(2)


      if (verbose_flag)  print *, "Calculating Mur ABC..."

!     Calculate Ey

      call calc_MurF_pt(Ey(imin-1:imin+2), Ey1n_xmin, imin-1, imin-1, +1, M)

      call calc_MurF_pt(Ey(imax-2:imax+1), Ey1n_xmax, imax-2, imax+1, -1, M)

      END SUBROUTINE calc_MurF_ABC

!-----------------------------------------------------------------------

!>    Calculates magnetic field values at spatial domain boundaries.
!!    Wrapper subroutine making calls to 'calc_MurF_pt' to update
!!    magnetic field values at x{min,max} points.
!!
!!    Method: Mur, 1st order Absorbing Boundary Conditions
!!    Scheme: Fang's (2,4) discretisation
!!
!!    @param[in] Hz              magnetic field values at time step (n)
!!    @param[in] Hz1n_x{min,max} magnetic field values at E(n-1)
!!    @param[in] imin, imax      spatial domain range indices
!!    @param[in] M

      SUBROUTINE calc_MurF_ABC_H4(Hz, Hz1n_xmin, Hz1n_xmax, &
                                  imin, imax, M)

      integer, intent(in)    :: imin, imax
      real(p), intent(inout) :: Hz(imin-1:)
      real(p), intent(in)    :: Hz1n_xmin(imin-1:), Hz1n_xmax(imax-2:)
      real(p), intent(in)    :: M(2)


      if (verbose_flag)  print *, "Calculating Mur ABC..."

!     Calculate Hz
!     Left  side (imin)
      call calc_MurF_pt(Hz(imin-1:imin+2), Hz1n_xmin, imin-1, imin-1, +1, M)

!     Right side (imax)
      call calc_MurF_pt(Hz(imax-2:imax+1), Hz1n_xmax(imax-2:imax+1), imax-2, imax+1, -1, M)
      call calc_MurF_pt(Hz(imax-1:imax+2), Hz1n_xmax(imax-1:imax+2), imax-1, imax+2, -1, M)

      END SUBROUTINE calc_MurF_ABC_H4

!-----------------------------------------------------------------------

!>    Calculates electric field values at spatial domain boundaries.
!!    Wrapper subroutine making calls to 'calc_MurF_pt' to update
!!    electric field values at x{min,max} points.
!!
!!    Method: Mur, 1st order Absorbing Boundary Conditions
!!    Scheme: Fang's (2,4) discretisation
!!
!!    @param[in] Ey              electric field values at time step (n)
!!    @param[in] Ey1n_x{min,max} electric field values at E(n-1)
!!    @param[in] imin, imax      spatial domain range indices
!!    @param[in] M

      SUBROUTINE calc_MurF_ABC_E4(Ey, Ey1n_xmin, Ey1n_xmax, &
                                  imin, imax, M)

      integer, intent(in)    :: imin, imax
      real(p), intent(inout) :: Ey(imin-2:)
      real(p), intent(in)    :: Ey1n_xmin(imin-2:), Ey1n_xmax(imax-2:)
      real(p), intent(in)    :: M(2)


      if (verbose_flag)  print *, "Calculating Mur ABC..."

!     Calculate Ey
!     Left  side (imin)
      call calc_MurF_pt(Ey(imin-1:imin+2), Ey1n_xmin(imin-1:imin+2), imin-1, imin-1, +1, M)
      call calc_MurF_pt(Ey(imin-2:imin+1), Ey1n_xmin(imin-2:imin+1), imin-2, imin-2, +1, M)

!     Right side (imax)
      call calc_MurF_pt(Ey(imax-2:imax+1), Ey1n_xmax, imax-2, imax+1, -1, M)

      END SUBROUTINE calc_MurF_ABC_E4

!-----------------------------------------------------------------------

!>    Re-arranges electric fields for 4th order spatial approx., i.e.
!!    saves given field values at current time step (n) as previous time
!!    step values (n-1).
!!
!!    @param[in] E, Eu1n_v{min,max} source and destination arrays
!!    @param[in] {i,j,k}{min,max}   spatial domain ranges

      SUBROUTINE re_arrange_E4(E, Ex1n_ymin, Ex1n_ymax, Ex1n_zmin, &
                               Ex1n_zmax, Ey1n_xmin, Ey1n_xmax,    &
                               Ey1n_zmin, Ey1n_zmax, Ez1n_xmin,    &
                               Ez1n_xmax, Ez1n_ymin, Ez1n_ymax,    &
                               imin, imax, jmin, jmax, kmin, kmax)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax

      real(p), intent(in) :: E(imin-1:,jmin-1:,kmin-1:,1:)

      real(p), intent(inout) :: Ex1n_ymin(imin-1:,jmin-1:,kmin-1:), &
                                Ex1n_ymax(imin-1:,jmax:,  kmin-1:), &
                                Ex1n_zmin(imin-1:,jmin-1:,kmin-1:), &
                                Ex1n_zmax(imin-1:,jmin-1:,kmax:)

      real(p), intent(inout) :: Ey1n_xmin(imin-1:,jmin-1:,kmin-1:), &
                                Ey1n_xmax(imax:,  jmin-1:,kmin-1:), &
                                Ey1n_zmin(imin-1:,jmin-1:,kmin-1:), &
                                Ey1n_zmax(imin-1:,jmin-1:,kmax:)

      real(p), intent(inout) :: Ez1n_xmin(imin-1:,jmin-1:,kmin-1:), &
                                Ez1n_xmax(imax:,  jmin-1:,kmin-1:), &
                                Ez1n_ymin(imin-1:,jmin-1:,kmin-1:), &
                                Ez1n_ymax(imin-1:,jmax:,  kmin-1:)


      if (verbose_flag) then

        print *, "Re-arranging electric field values..."

      end if

      ! Re-arrange Ex @ y{min,max}
      Ex1n_ymin(imin-1:imax+2, jmin-1:jmin, kmin-1:kmax+2) = &
              E(imin-1:imax+2, jmin-1:jmin, kmin-1:kmax+2, 1)

      Ex1n_ymax(imin-1:imax+2, jmax:jmax+2, kmin-1:kmax+2) = &
              E(imin-1:imax+2, jmax:jmax+2, kmin-1:kmax+2, 1)

      ! Re-arrange Ex @ z{min,max}
      Ex1n_zmin(imin-1:imax+2, jmin-1:jmin+2, kmin-1:kmin) = &
              E(imin-1:imax+2, jmin-1:jmin+2, kmin-1:kmin, 1)

      Ex1n_zmax(imin-1:imax+2, jmax-1:jmax+2, kmax:kmax+2) = &
              E(imin-1:imax+2, jmax-1:jmax+2, kmax:kmax+2, 1)


      ! Re-arrange Ey @ x{min,max}
      Ey1n_xmin(imin-1:imin, jmin-1:jmax+2, kmin-1:kmax+2) = &
              E(imin-1:imin, jmin-1:jmax+2, kmin-1:kmax+2, 2)

      Ey1n_xmax(imax:imax+2, jmin-1:jmax+2, kmin-1:kmax+2) = &
              E(imax:imax+2, jmin-1:jmax+2, kmin-1:kmax+2, 2)

      ! Re-arrange Ey @ z{min,max}
      Ey1n_zmin(imin-1:imax+2, jmin-1:jmax+2, kmin-1:kmin) = &
              E(imin-1:imax+2, jmin-1:jmax+2, kmin-1:kmin, 2)

      Ey1n_zmax(imin-1:imax+2, jmin-1:jmax+2, kmax:kmax+2) = &
              E(imin-1:imax+2, jmin-1:jmax+2, kmax:kmax+2, 2)


      ! Re-arrange Ez @ x{min,max}
      Ez1n_xmin(imin-1:imin, jmin-1:jmax+2, kmin-1:kmax+2) = &
              E(imin-1:imin, jmin-1:jmax+2, kmin-1:kmax+2, 3)

      Ez1n_xmax(imax:imax+2, jmin-1:jmax+2, kmin-1:kmax+2) = &
              E(imax:imax+2, jmin-1:jmax+2, kmin-1:kmax+2, 3)

      ! Re-arrange Ez @ y{min,max}
      Ez1n_ymin(imin-1:imax+2, jmin-1:jmin, kmin-1:kmax+2) = &
              E(imin-1:imax+2, jmin-1:jmin, kmin-1:kmax+2, 3)

      Ez1n_ymax(imin-1:imax+2, jmax:jmax+2, kmin-1:kmax+2) = &
              E(imin-1:imax+2, jmax:jmax+2, kmin-1:kmax+2, 3)

      END SUBROUTINE re_arrange_E4

!-----------------------------------------------------------------------

!>    Re-arranges magnetic fields for 4th order spatial approx., i.e.
!!    saves given field values at current time step (n) as previous time
!!    step values (n-1).
!!
!!    @param[in] H, Hu1n_v{min,max} source and destination arrays
!!    @param[in] {i,j,k}{min,max}   spatial domain ranges

      SUBROUTINE re_arrange_H4(H, Hx1n_ymin, Hx1n_ymax, Hx1n_zmin, &
                                  Hx1n_zmax, Hy1n_xmin, Hy1n_xmax, &
                                  Hy1n_zmin, Hy1n_zmax, Hz1n_xmin, &
                                  Hz1n_xmax, Hz1n_ymin, Hz1n_ymax, &
                               imin, imax, jmin, jmax, kmin, kmax)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax

      real(p), intent(in) :: H(imin-2:,jmin-2:,kmin-2:,1:)

      real(p), intent(inout) :: Hx1n_ymin(imin-2:,jmin-2:,kmin-2:), &
                                Hx1n_ymax(imin-2:,jmax:,  kmin-2:), &
                                Hx1n_zmin(imin-2:,jmin-2:,kmin-2:), &
                                Hx1n_zmax(imin-2:,jmin-2:,kmax:)

      real(p), intent(inout) :: Hy1n_xmin(imin-2:,jmin-2:,kmin-2:), &
                                Hy1n_xmax(imax:,  jmin-2:,kmin-2:), &
                                Hy1n_zmin(imin-2:,jmin-2:,kmin-2:), &
                                Hy1n_zmax(imin-2:,jmin-2:,kmax:)

      real(p), intent(inout) :: Hz1n_xmin(imin-2:,jmin-2:,kmin-2:), &
                                Hz1n_xmax(imax:,  jmin-2:,kmin-2:), &
                                Hz1n_ymin(imin-2:,jmin-2:,kmin-2:), &
                                Hz1n_ymax(imin-2:,jmax:,  kmin-2:)


      if (verbose_flag) then

        print *, "Re-arranging magnetic field values..."

      end if

      ! Re-arrange Hx @ y{min,max}
      Hx1n_ymin(imin-2:imax+1, jmin-2:jmin, kmin-2:kmax+1) = &
              H(imin-2:imax+1, jmin-2:jmin, kmin-2:kmax+1, 1)

      Hx1n_ymax(imin-2:imax+1, jmax:jmax+1, kmin-2:kmax+1) = &
              H(imin-2:imax+1, jmax:jmax+1, kmin-2:kmax+1, 1)

      ! Re-arrange Hx @ z{min,max}
      Hx1n_zmin(imin-2:imax+1, jmin-2:jmin+1, kmin-2:kmin) = &
              H(imin-2:imax+1, jmin-2:jmin+1, kmin-2:kmin, 1)

      Hx1n_zmax(imin-2:imax+1, jmax-2:jmax+1, kmax:kmax+1) = &
              H(imin-2:imax+1, jmax-2:jmax+1, kmax:kmax+1, 1)


      ! Re-arrange Hy @ x{min,max}
      Hy1n_xmin(imin-2:imin, jmin-2:jmax+1, kmin-2:kmax+1) = &
              H(imin-2:imin, jmin-2:jmax+1, kmin-2:kmax+1, 2)

      Hy1n_xmax(imax:imax+1, jmin-2:jmax+1, kmin-2:kmax+1) = &
              H(imax:imax+1, jmin-2:jmax+1, kmin-2:kmax+1, 2)

      ! Re-arrange Hy @ z{min,max}
      Hy1n_zmin(imin-2:imax+1, jmin-2:jmax+1, kmin-2:kmin) = &
              H(imin-2:imax+1, jmin-2:jmax+1, kmin-2:kmin, 2)

      Hy1n_zmax(imin-2:imax+1, jmin-2:jmax+1, kmax:kmax+1) = &
              H(imin-2:imax+1, jmin-2:jmax+1, kmax:kmax+1, 2)


      ! Re-arrange Hz @ x{min,max}
      Hz1n_xmin(imin-2:imin, jmin-2:jmax+1, kmin-2:kmax+1) = &
              H(imin-2:imin, jmin-2:jmax+1, kmin-2:kmax+1, 3)

      Hz1n_xmax(imax:imax+1, jmin-2:jmax+1, kmin-2:kmax+1) = &
              H(imax:imax+1, jmin-2:jmax+1, kmin-2:kmax+1, 3)

      ! Re-arrange Hz @ y{min,max}
      Hz1n_ymin(imin-2:imax+1, jmin-2:jmin, kmin-2:kmax+1) = &
              H(imin-2:imax+1, jmin-2:jmin, kmin-2:kmax+1, 3)

      Hz1n_ymax(imin-2:imax+1, jmax:jmax+1, kmin-2:kmax+1) = &
              H(imin-2:imax+1, jmax:jmax+1, kmin-2:kmax+1, 3)

      END SUBROUTINE re_arrange_H4

!-----------------------------------------------------------------------

      END MODULE MUR_FANG

!-----------------------------------------------------------------------
! EoF: mur_fang.f90
!-----------------------------------------------------------------------
