!-----------------------------------------------------------------------
!
!>    @brief Contains constants used in 3D FDTD code.
!!
!!    @author  Maksims Abalenkovs
!!    @email   maksims.abalenkovs@stfc.ac.uk
!!    @date    Oct 31, 2023
!!    @version 0.6
!
!-----------------------------------------------------------------------

      MODULE CONST

!--------------------------------------------------------------------

      USE PRCSN

      IMPLICIT NONE

!--------------------------------------------------------------------

!>    Speed of light [m/s]
      real(p), parameter :: C0 = 299792458

!>    Output, timing, verification file names
      character(len=10), parameter :: outfile = "output.txt"
      character(len=10), parameter :: timfile = "timing.txt"
      character(len=10), parameter :: verfile = "ipulse.txt"

!>    Verification, timing, output file handles (unit numbers)
      integer, parameter :: oh = 251
      integer, parameter :: th = 252
      integer, parameter :: vh = 253

!>    Pi
      real(p), parameter :: PI = acos(-1.0)

!-----------------------------------------------------------------------

      END MODULE CONST

!-----------------------------------------------------------------------
!!    @eof const.f90
!-----------------------------------------------------------------------
