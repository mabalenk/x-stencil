!-----------------------------------------------------------------------
!
!>    @brief Contains initialisation subroutine declarations.
!!
!!    @author  Maksims Abalenkovs
!!    @email   maksims.abalenkovs@stfc.ac.uk
!!    @date    Oct 31, 2023
!!    @version 0.15
!!
!!    Subroutines (6):
!!
!!        alloc_data_arrays()
!!        init_data_arrays()
!!        init_sim()
!!        fin_sim()
!!        apply_env_slab()
!!        init_radio_env()
!
!-----------------------------------------------------------------------

      MODULE INIT

      USE ALLOC
      USE CONST
      USE IO
      USE MUR
      USE OMP_LIB
      USE VARIABLES
      USE YEE

      IMPLICIT NONE

!-----------------------------------------------------------------------

      CONTAINS

!-----------------------------------------------------------------------

!>    Allocates data arrays
      SUBROUTINE alloc_data_arrays

      if (verbose_flag) then

          print *, "Allocating data arrays..."
          print *, "Allocating magnetic field array..."

      end if

      ! Allocate magnetic field arrays
!       allocate(Hx(imin  :imax, jmin-1:jmax, kmin-1:kmax),         &
!                Hy(imin-1:imax, jmin  :jmax, kmin-1:kmax),         &
!                Hz(imin-1:imax, jmin-1:jmax, kmin  :kmax), stat=err)

      select case (ord)

          case (2)
              allocate(H(imin-1:imax, jmin-1:jmax, kmin-1:kmax, 3), stat=err)

          case (4)
              allocate(H(imin-2:imax+1, jmin-2:jmax+1, kmin-2:kmax+1, 3), stat=err)

      end select

      call check_alloc_act("allocate", "magnetic field", err)

      if (verbose_flag) then

          print *, "Allocating electric field arrays..."

      end if

      select case (ord)

          case (2)
              allocate(E(imin:imax+1, jmin:jmax+1, kmin:kmax+1, 3), stat=err)

          case (4)
              allocate(E(imin-1:imax+2, jmin-1:jmax+2, kmin-1:kmax+2, 3), stat=err)

      end select

      call check_alloc_act("allocate", "electric field", err)

      if (abc_type == "mur1") then

          if (verbose_flag) then
  
              print *, "Allocating electric field values at time step (n-1)"
  
          end if
  
          select case (ord)

              case(2)

                  allocate(Ex1n_ymin(imin:imax+1,jmin:jmin+1,kmin:kmax+1), stat=err)
                  allocate(Ex1n_ymax(imin:imax+1,jmax:jmax+1,kmin:kmax+1), stat=err)
    
                  allocate(Ex1n_zmin(imin:imax+1,jmin:jmax+1,kmin:kmin+1), stat=err)
                  allocate(Ex1n_zmax(imin:imax+1,jmin:jmax+1,kmax:kmax+1), stat=err)
    
                  call check_alloc_act("allocate",                               &
                                       "electric field x-component at (n-1)", err)
        
                  allocate(Ey1n_xmin(imin:imin+1,jmin:jmax+1,kmin:kmax+1), stat=err)
                  allocate(Ey1n_xmax(imax:imax+1,jmin:jmax+1,kmin:kmax+1), stat=err)
        
                  allocate(Ey1n_zmin(imin:imax+1,jmin:jmax+1,kmin:kmin+1), stat=err)
                  allocate(Ey1n_zmax(imin:imax+1,jmin:jmax+1,kmax:kmax+1), stat=err)
        
                  call check_alloc_act("allocate",                               &
                                       "electric field y-component at (n-1)", err)
        
                  allocate(Ez1n_xmin(imin:imin+1,jmin:jmax+1,kmin:kmax+1), stat=err)
                  allocate(Ez1n_xmax(imax:imax+1,jmin:jmax+1,kmin:kmax+1), stat=err)
        
                  allocate(Ez1n_ymin(imin:imax+1,jmin:jmin+1,kmin:kmax+1), stat=err)
                  allocate(Ez1n_ymax(imin:imax+1,jmax:jmax+1,kmin:kmax+1), stat=err)
        
                  call check_alloc_act("allocate",                               &
                                       "electric field z-component at (n-1)", err)

              case(4)

                  allocate(Hx1n_ymin(imin-2:imax+1,jmin-2:jmin,kmin-2:kmax+1), stat=err)
                  allocate(Hx1n_ymax(imin-2:imax+1,jmax:jmax+1,kmin-2:kmax+1), stat=err)
          
                  allocate(Hx1n_zmin(imin-2:imax+1,jmin-2:jmax+1,kmin-2:kmin), stat=err)
                  allocate(Hx1n_zmax(imin-2:imax+1,jmin-2:jmax+1,kmax:kmax+1), stat=err)
          
                  call check_alloc_act("allocate",                               &
                                       "magnetic field x-component at (n-1)", err)
          
                  allocate(Hy1n_xmin(imin-2:imin,jmin-2:jmax+1,kmin-2:kmax+1), stat=err)
                  allocate(Hy1n_xmax(imax:imax+1,jmin-2:jmax+1,kmin-2:kmax+1), stat=err)
          
                  allocate(Hy1n_zmin(imin-2:imax+1,jmin-2:jmax+1,kmin-2:kmin), stat=err)
                  allocate(Hy1n_zmax(imin-2:imax+1,jmin-2:jmax+1,kmax:kmax+1), stat=err)
          
                  call check_alloc_act("allocate",                               &
                                       "magnetic field y-component at (n-1)", err)
      
                  allocate(Hz1n_xmin(imin-2:imin,jmin-2:jmax+1,kmin-2:kmax+1), stat=err)
                  allocate(Hz1n_xmax(imax:imax+1,jmin-2:jmax+1,kmin-2:kmax+1), stat=err)
      
                  allocate(Hz1n_ymin(imin-2:imax+1,jmin-2:jmin,kmin-2:kmax+1), stat=err)
                  allocate(Hz1n_ymax(imin-2:imax+1,jmax:jmax+1,kmin-2:kmax+1), stat=err)
          
                  call check_alloc_act("allocate",                               &
                                       "magnetic field z-component at (n-1)", err)
      
      
                  allocate(Ex1n_ymin(imin-1:imax+2,jmin-1:jmin,kmin-1:kmax+2), stat=err)
                  allocate(Ex1n_ymax(imin-1:imax+2,jmax:jmax+2,kmin-1:kmax+2), stat=err)
          
                  allocate(Ex1n_zmin(imin-1:imax+2,jmin-1:jmax+2,kmin-1:kmin), stat=err)
                  allocate(Ex1n_zmax(imin-1:imax+2,jmin-1:jmax+2,kmax:kmax+2), stat=err)
          
                  call check_alloc_act("allocate",                               &
                                       "electric field x-component at (n-1)", err)
          
                  allocate(Ey1n_xmin(imin-1:imin,jmin-1:jmax+2,kmin-1:kmax+2), stat=err)
                  allocate(Ey1n_xmax(imax:imax+2,jmin-1:jmax+2,kmin-1:kmax+2), stat=err)
          
                  allocate(Ey1n_zmin(imin-1:imax+2,jmin-1:jmax+2,kmin-1:kmin), stat=err)
                  allocate(Ey1n_zmax(imin-1:imax+2,jmin-1:jmax+2,kmax:kmax+2), stat=err)
          
                  call check_alloc_act("allocate",                               &
                                       "electric field y-component at (n-1)", err)
          
                  allocate(Ez1n_xmin(imin-1:imin,jmin-1:jmax+2,kmin-1:kmax+2), stat=err)
                  allocate(Ez1n_xmax(imax:imax+2,jmin-1:jmax+2,kmin-1:kmax+2), stat=err)
      
                  allocate(Ez1n_ymin(imin-1:imax+2,jmin-1:jmin,kmin-1:kmax+2), stat=err)
                  allocate(Ez1n_ymax(imin-1:imax+2,jmax:jmax+2,kmin-1:kmax+2), stat=err)
          
                  call check_alloc_act("allocate",                               &
                                       "electric field z-component at (n-1)", err)
              case default

                  stop "Error: incorrect order of spatial discretisation!"

          end select

      end if


      if (verbose_flag) then

          print *, "Allocating materials array..."

      end if

      allocate( mats(nmats), stat=err )

      call check_alloc_act("allocate", "materials", err)


      if (verbose_flag) then

          print *, "Allocating material encoding location arrays.."

      end if

      allocate( enc(imin:imax, jmin:jmax, kmin:kmax), stat=err )

      call check_alloc_act("allocate", "encodings", err)


      if (verbose_flag) then

          print *, "Allocating material coefficient arrays..."

      end if

      allocate( G(nmats,2), Z(nmats,2), stat=err )

      call check_alloc_act("allocate", "coefficients", err)


      if (pre_flag) then

          if (verbose_flag) then
  
              print *, "Allocating material coefficient matrices..."
  
          end if
  
          allocate( Gm(imin:imax, jmin:jmax, kmin:kmax, ord+1),          &
                    Zm(imin:imax, jmin:jmax, kmin:kmax, ord+1), stat=err )
  
          call check_alloc_act("allocate", "coefficient matrices", err)

      end if


      if (verbose_flag) then

          print *, "Allocating current density array..."

      end if

      allocate( Jz(n0:n1), stat=err )

      call check_alloc_act("allocate", "current density", err)

      END SUBROUTINE alloc_data_arrays

!-----------------------------------------------------------------------

!>    Initialise data arrays
      SUBROUTINE init_data_arrays

      if (verbose_flag) then

          print *, "Initialising data arrays..."
          print *, "Initialising magnetic field arrays..."

      end if

!       Hx(:,:,:) = 0.0d+0
!       Hy(:,:,:) = 0.0d+0
!       Hz(:,:,:) = 0.0d+0

!       call random_number(Hx)
!       call random_number(Hy)
!       call random_number(Hz)

      H(:,:,:,:) = 0.0d+0

!       call random_number(H)


      if (verbose_flag) then

          print *, "Initialising electric field arrays..."

      end if

!       Ex(:,:,:) = 0.0d+0
!       Ey(:,:,:) = 0.0d+0
!       Ez(:,:,:) = 0.0d+0

!       call random_number(Ex)
!       call random_number(Ey)
!       call random_number(Ez)

      E(:,:,:,:) = 0.0d+0

!       call random_number(E)

      if (abc_type == "mur1") then

          if (verbose_flag) then

              print *, "Initialising magnetic field values at (n-1)..."

          end if

          Hx1n_ymin = 0.0d+0; Hx1n_ymax = 0.0d+0
          Hx1n_zmin = 0.0d+0; Hx1n_zmax = 0.0d+0
  
          Hy1n_xmin = 0.0d+0; Hy1n_xmax = 0.0d+0
          Hy1n_zmin = 0.0d+0; Hy1n_zmax = 0.0d+0
  
          Hz1n_xmin = 0.0d+0; Hz1n_xmax = 0.0d+0
          Hz1n_ymin = 0.0d+0; Hz1n_ymax = 0.0d+0


          if (verbose_flag) then
  
              print *, "Initialising eletric field values at (n-1)..."
  
          end if
  
          Ex1n_ymin = 0.0d+0; Ex1n_ymax = 0.0d+0
          Ex1n_zmin = 0.0d+0; Ex1n_zmax = 0.0d+0
  
          Ey1n_xmin = 0.0d+0; Ey1n_xmax = 0.0d+0
          Ey1n_zmin = 0.0d+0; Ey1n_zmax = 0.0d+0
  
          Ez1n_xmin = 0.0d+0; Ez1n_xmax = 0.0d+0
          Ez1n_ymin = 0.0d+0; Ez1n_ymax = 0.0d+0

      end if


      if (verbose_flag) then

          print *, "Initialising material coefficient arrays..."

      end if

      G = 0.0d+0
      Z = 0.0d+0

!       call random_number(G)
!       call random_number(Z)


      if (pre_flag) then

          if (verbose_flag) then
    
              print *,     "Initialising material coefficient matrices..."
    
          end if
    
          Gm = 0.0
          Zm = 0.0

!           call random_number(Gm)
!           call random_number(Zm)

      end if


      if (verbose_flag) then

          print *,     "Initialising materials array..."

      end if

      mats(:) = material("not assigned", 0.0, 0.0, 0.0, 0.0)


      if (verbose_flag) then

          print *,     "Initialising material code location array..."

      end if

      enc(:,:,:) = 0


      if (verbose_flag) then

          print *,     "Initialising current density array..."

      end if

      Jz(:) = 0.0d+0

      END SUBROUTINE init_data_arrays

!-----------------------------------------------------------------------

!>    Initialises parameters and variables, allocates and initialises data arrays

      SUBROUTINE init_sim

      USE MUR

      ! Source point counter
      integer :: i

      ! Output file header
      character(len=:), allocatable :: hdr


      if (verbose_flag) then

          print *, "Initialising parameters and variables..."

      end if

      lambda = C0/wfreq

      dx = lambda/resolution
      dy = lambda/resolution
      dz = lambda/resolution

      ! use standard Courant-Friedrichs-Lewy criterion
      dt = calc_dt(ncfl, C0, dx, dy, dz, ord, 3)

      mu_0  = 4.0d+0*PI*10.0d+0**(-7.0d+0)
      eps_0 = 1.0d+0/(mu_0*C0**2.0d+0)

      if (norm_flag) then
          iota = sqrt(eps_0/mu_0)
      else
          iota = real(1,p)
      end if

!     Set start and end indices of current density array
      n0 = 1

!     Calculate end index of current density array,
!     Power of two number nearest to 'tmax'
      n1 = 2**ceiling( log(real(tmax)) / log(2.0) )

!     Allocate data arrays
      call alloc_data_arrays

!     Initialise data arrays
      call init_data_arrays

!     Open output files
      hdr = "#       Hx     |     Hy     |     Hz     |&
               &     Ex     |     Ey     |     Ez"
      call open_out_file(oh, outfile, "(A)", hdr)

      call open_out_file(th, timfile, "(A)", "#  nthr  |  t_calc")

      call open_out_file(vh, verfile, "(A)", "#  n  |  Jn")

      ! Calculate coefficients for Mur ABC
      ! @note Use Mur1 coefficients for "stacked" Mur 1st with Fang (2,4)
      if ((abc_type == "mur1").or.(abc_type == "mur2")) then

          call calc_Mur_coeffs(C0, dx, dt, 2, Mx)
          call calc_Mur_coeffs(C0, dy, dt, 2, My)
          call calc_Mur_coeffs(C0, dz, dt, 2, Mz)

      end if

!     Print initialised parameters and variables
      print *, "eps_0:     ", eps_0
      print *, "mu_0:      ", mu_0
      print *, "iota:      ", iota
      print *, "pi:        ", PI
      print *, ""
      print *, "lambda:    ", lambda
      print *, "tmax:      ", tmax
      print *, "n0, n1:    ", n0, n1
      print *, ""

      print *, "dx, dy, dz:", dx, dy, dz
      print *, "ncfl:      ", ncfl
      print *, "dt:        ", dt
      print *, ""

      print "(A, I4.1)", "nsrc: ", nsrc

      do i = 1, nsrc
          print "(3(3X, I5.1))", src(i)
      end do

      print *, ""

      !$omp parallel
      !$omp single
      nthr = omp_get_num_threads()
      !$omp end single
      !$omp end parallel

      print *, "nthr: ", nthr

      END SUBROUTINE init_sim

!-----------------------------------------------------------------------

!>    Finalise parameters and variables

      SUBROUTINE fin_sim

      if (verbose_flag) then

          print *, "Finalising parameters and variables..."
          print *, "Deallocating excitation source locations array..."

      end if

      deallocate( src, stat=err )


      call check_alloc_act("deallocate",                     &
                           "excitation source locations", err)


      if (verbose_flag) then

          print *, "Deallocating material slabs array..."

      end if


      if (verbose_flag) then

          print *, "Deallocating magnetic field array..."

      end if

      ! deallocate( Hx, Hy, Hz, stat=err )
      deallocate( H, stat=err )

      call check_alloc_act("deallocate", "magnetic field", err)


      if (verbose_flag) then

          print *, "Deallocating electric field array..."

      end if

      ! deallocate( Ex, Ey, Ez, stat=err )
      deallocate( E, stat=err )

      call check_alloc_act("deallocate", "electric field", err)


      if ((abc_type == "mur1").or.(abc_type == "mur2")) then

          if (verbose_flag) then
    
              print *, "Deallocating magnetic field at time step (n-1)..."
    
          end if
    
          deallocate( Hx1n_ymin, Hx1n_ymax, Hx1n_zmin, Hx1n_zmax, &
                      Hy1n_xmin, Hy1n_xmax, Hy1n_zmin, Hy1n_zmax, &
                      Hz1n_xmin, Hz1n_xmax, Hz1n_ymin, Hz1n_ymax, &
                      stat=err )
    
          call check_alloc_act("deallocate", "electric field at (n-1)", err)
  
  
          if (verbose_flag) then
    
              print *, "Deallocating electric field at time step (n-1)..."
    
          end if
    
          deallocate( Ex1n_ymin, Ex1n_ymax, Ex1n_zmin, Ex1n_zmax, &
                      Ey1n_xmin, Ey1n_xmax, Ey1n_zmin, Ey1n_zmax, &
                      Ez1n_xmin, Ez1n_xmax, Ez1n_ymin, Ez1n_ymax, &
                      stat=err )
    
          call check_alloc_act("deallocate", "electric field at (n-1)", err)

      end if


      if (verbose_flag) then

          print *, "Deallocating materials array..."

      end if

      deallocate( mats, stat=err )

      call check_alloc_act("deallocate", "materials", err)


      if (pre_flag) then

          if (verbose_flag) then
    
              print *, "Deallocating material coefficient matrices..."
    
          end if
    
          deallocate( Gm, Zm, stat=err )
    
          call check_alloc_act("deallocate",                       &
                               "material coefficient matrices", err)
      else

          call dealloc_coeff_arrays

      end if


      if (verbose_flag) then

          print *, "Deallocating current density..."

      end if


      deallocate( Jz, stat=err )

      call check_alloc_act("deallocate", "current density", err)


!     Close output file
      call close_out_file(oh, outfile)
      call close_out_file(th, timfile)
      call close_out_file(vh, verfile)

      print *, ""

      END SUBROUTINE fin_sim

!-----------------------------------------------------------------------

!>    Apply radio environment onto specified hyperslab
!!    Enforces material code 'mcode' over coordinate range of 'enc'
!!
!!    @param[in] d material hyperslab

      SUBROUTINE apply_env_slab(d)

      type(hyperslab), intent(in) :: d

!     Local variable
!     Verification output string format
      character(len=*), parameter :: frmt = "(A, I3.1, 2A, 6(I5.1))"


      if (verbose_flag) then

          print *, "Setting main grid hyperslab..."
  
      end if

      enc(d%x0:d%x1, d%y0:d%y1, d%z0:d%z1) = d%mcode

      print frmt, "Radio environment set successfully: ",      &
            d%mcode, ", ", trim(adjustl(mats(d%mcode)%mdesc)), &
            d%x0, d%x1, d%y0, d%y1, d%z0, d%z1

      print *, ""

      END SUBROUTINE apply_env_slab

!-----------------------------------------------------------------------

!>    Reads material and environment specifications from files
!!    and sets radio environment for entire simulation space
!!
!!    Wrapper subroutine

      SUBROUTINE init_radio_env

      if (verbose_flag) then

          print *, "Initialising radio environment..."

      end if

!     Read material specifications from file
      call read_mat_specs(53, "mat_specs")

!     Initialise entire simulation space with "Air" environment
      call apply_env_slab(hyperslab(1, imin, imax, jmin, jmax, kmin, kmax))

!     @test Output material encoding locations into files
!       call output_enc( mats, enc, 54, "enc.a",            &
!                        imin, imax, jmin, jmax, kmin, kmax )

      END SUBROUTINE init_radio_env

!-----------------------------------------------------------------------

      END MODULE INIT

!-----------------------------------------------------------------------
!!    @eof  init.f90
!-----------------------------------------------------------------------
