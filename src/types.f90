!-----------------------------------------------------------------------
!
!>    @brief Implements prototype of stencil calculation in 3D domain
!!           over given number of time iterations.
!!
!!    @author  Maksims Abalenkovs
!!    @email   m.abalenkovs@manchester.ac.uk
!!    @date    Apr 3, 2017
!!    @version 0.4
!!
!!    @brief Contains custom data types used in 3D code
!
!-----------------------------------------------------------------------

      MODULE TYPES

!-----------------------------------------------------------------------

      USE PRCSN

      IMPLICIT NONE

!-----------------------------------------------------------------------

!>    Excitation source point
!!      @param x, y, z point location
      type point
        integer :: x, y, z
      end type point

!>    Material encoding
!!
!!    Material specification
!!      @param mdesc material verbal description
!!      @param sigma electric conductivity    [S/m]
!!      @param eps_r relative permittivity    [-]
!!      @param rho   equivalent magnetic loss [ohm/m]
!!      @param mu_r  relative permeability    [-]

      type material
        character(len=47) :: mdesc
        real(p) :: sigma
        real(p) :: eps_r
        real(p) :: rho
        real(p) :: mu_r
      end type material

!>    Material hyperslab specification
!!      @param mcode  material code
!!      @param x0..x1 material hyperslab span in x-axis
!!      @param y0..y1 material hyperslab span in y-axis
!!      @param z0..z1 material hyperslab span in z-axis

      type hyperslab
        integer   :: mcode
        integer   :: x0, x1, y0, y1, z0, z1
      end type hyperslab

!-----------------------------------------------------------------------

      END MODULE TYPES

!-----------------------------------------------------------------------
