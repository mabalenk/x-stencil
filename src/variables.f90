!-----------------------------------------------------------------------
!
!>    @brief Implements prototype of stencil calculation in 3D domain
!!           over given number of time iterations.
!!
!!    @author  Maksims Abalenkovs
!!    @email   maksims.abalenkovs@stfc.ac.uk
!!    @date    Oct 31, 2023
!!    @version 0.8
!
!-----------------------------------------------------------------------

      MODULE VARIABLES
      
      USE PRCSN
      USE TYPES
      
      IMPLICIT NONE
      SAVE

!--------------------------------------------------------------------

!>    Number of OpenMP threads
      integer :: nthr

!>    Time step counter
      integer :: t

!>    Total number of simulation time steps
      integer :: tmax

!>    Temporal increment
      real(p) :: dt

!>    Courant-Friedrichs-Lewy Number
      real(p) :: ncfl

!>    Approximation order for space derivatives
      integer :: ord  ! 2, 4, 6, 8, 10

!>    Spatial increments
      real(p) :: dx, dy, dz

!>    Domain boundary coordinates
      integer :: imin, imax, jmin, jmax, kmin, kmax

!-----------------------------------------------------------------------

!>    Physical Variables

!>    Wave frequency [Hz]
      real(p) :: wfreq

!>    Wavelength [m]
      real(p) :: lambda

!>    Resolution (Grid points per wavelength)
      integer :: resolution

!-----------------------------------------------------------------------

!>    Number of excitation source and observation points
      integer :: nsrc, nobs

!>    Excitation source array
      type(point), allocatable :: src(:)  ! src(nsrc)

!>    Observation point arrays
      type(point), allocatable :: obs(:)

!-----------------------------------------------------------------------

!>    Excitation source type: hard/soft
      character(len=4) :: src_type
 
!-----------------------------------------------------------------------

!>    Media parameters

!>    Permeability of free space, mu_0 [H/m]
      real(p) :: mu_0

!>    Permittivity of free space, epsilon_0 [F/m]
      real(p) :: eps_0

!>    Normalisation parameter for electromagnetic coefficients G2, Z2
      real(p) :: iota

!-----------------------------------------------------------------------

!>    Field variables

!>    @param Wr current time step values (n), where W in [E,H], r in [x,y,z]

!>    Magnetic field components, H{x,y,z}
      ! real(p), allocatable :: Hx(:,:,:), Hy(:,:,:), Hz(:,:,:)
      real(p), allocatable :: H(:,:,:,:)

!>    Electric field components, E{x,y,z}
      ! real(p), allocatable :: Ex(:,:,:), Ey(:,:,:), Ez(:,:,:)
      real(p), allocatable :: E(:,:,:,:)

!-----------------------------------------------------------------------

!>    Electric current density [A/m^2]
      real(p), allocatable :: Jz(:)

!>    Density array begin and end indices
      integer :: n0, n1

!-----------------------------------------------------------------------

!>    Flag values: T -- true (yes), F -- false (no)

!>    Verbosity flag (provide maximum execution status information)
      logical :: verbose_flag

!>    Impulse response
      logical :: impulse_flag

!>    Enable normalisation of electromagnetic field coefficients G2, Z2
      logical :: norm_flag

!>    Use pre-computed values for coefficients Gamma and Zeta (G/Z) in
!!    matrix form
      logical :: pre_flag

!>    Type of FDTD update equation: expl, func, dotp, ddot, matm, dgmv, dgmm
      character(len=4) :: eqtype

!>    Error code
      integer :: err

!>    Info message
      character(len=128) :: msg

!-----------------------------------------------------------------------

!>    Material encoding

!>    No. of materials
      integer :: nmats

!>    Material code
      integer :: mcode

!>    Materials array
      type(material), allocatable :: mats(:)  ! mats(nmats)

!-----------------------------------------------------------------------

!>    Material encoding locations arrays
      integer, allocatable :: enc(:,:,:)

!>    Material coefficients
!!      @param G coefficients for electric field calculation
!!      @param Z coefficients for magnetic field calculation

      real(p), allocatable :: G(:,:), Z(:,:)  !< G(nmats,2), Z(nmats,2)

!>    Material coefficient matrices
!!      Gm(imin:imax, jmin:jmax, kmin:kmax, 1:3)
!!      Zm(imin:imax, jmin:jmax, kmin:kmax, 1:3)
      real(p), allocatable :: Gm(:,:,:,:), Zm(:,:,:,:)

!-----------------------------------------------------------------------

!>    ABC type: mur1/mur2
      character(len=4) :: abc_type

!-----------------------------------------------------------------------

!>    OpenMP timer variables
      real(dp) :: t0 = 0.0, t1 = 0.0, time = 0.0

!-----------------------------------------------------------------------

!>    OpenMP file output
      character(len= 17) :: ompname = ""
      character(len= 16) :: omp_time_str

!>    System command
      character(len=128) :: cmd     = ""

!-----------------------------------------------------------------------

      END MODULE VARIABLES

!-----------------------------------------------------------------------
!!    @eof  variables.f90
!-----------------------------------------------------------------------
