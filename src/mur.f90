!-----------------------------------------------------------------------
!
!     Implements prototype of stencil calculation in 3D domain
!     over given number of time iterations.
!
!>    @author  Maksims Abalenkovs
!!    @email   m.abalenkovs@manchester.ac.uk
!!    @date    Sep 12, 2017
!!    @version 0.11
!!
!!    @brief Contains subroutines for Mur,
!!           1st order Absorbing Boundary Conditions
!!
!!    Function:
!!
!!      Mur_pt()
!!
!!    Subroutines (3):
!!
!!      calc_Mur_pt()
!!      calc_Mur_ABC()
!!      calc_Mur_coeffs()
!!      re_arrange()
!
!-----------------------------------------------------------------------

      MODULE MUR

      USE CONST
      USE VARIABLES

      IMPLICIT NONE

!>    Coefficients for calculation of Mur Absorbing Boundary Conditions
      real(p) :: Mx(2), My(2), Mz(2)

!>    Magnetic field values at time step (n-1)
      real(p), allocatable :: Hx1n_ymin(:,:,:), Hx1n_ymax(:,:,:), &
                              Hx1n_zmin(:,:,:), Hx1n_zmax(:,:,:)

      real(p), allocatable :: Hy1n_xmin(:,:,:), Hy1n_xmax(:,:,:), &
                              Hy1n_zmin(:,:,:), Hy1n_zmax(:,:,:)

      real(p), allocatable :: Hz1n_xmin(:,:,:), Hz1n_xmax(:,:,:), &
                              Hz1n_ymin(:,:,:), Hz1n_ymax(:,:,:)

!>    Electric field values at time step (n-1)
      real(p), allocatable :: Ex1n_ymin(:,:,:), Ex1n_ymax(:,:,:), &
                              Ex1n_zmin(:,:,:), Ex1n_zmax(:,:,:)

      real(p), allocatable :: Ey1n_xmin(:,:,:), Ey1n_xmax(:,:,:), &
                              Ey1n_zmin(:,:,:), Ey1n_zmax(:,:,:)

      real(p), allocatable :: Ez1n_xmin(:,:,:), Ez1n_xmax(:,:,:), &
                              Ez1n_ymin(:,:,:), Ez1n_ymax(:,:,:)

!-----------------------------------------------------------------------

      CONTAINS

!-----------------------------------------------------------------------

!>    Calculates electric field value for given location at spatial 
!!    domain boundary.
!!    Method: Mur, 1st order Absorbing Boundary Conditions
!!
!!    @param[in] E1ni1, Eni1, E1ni electric field values at E(n-1,i+1),
!!                                                          E(n,  i+1),
!!                                                          E(n-1,i)
!!    @param[in] M coefficient for calculation of 1st order Mur ABC

      real(p) FUNCTION Mur_pt(E1ni1, Eni1, E1ni, M)

      real(p), intent(in) :: E1ni1, Eni1, E1ni, M

      Mur_pt = E1ni1 + M * (Eni1 - E1ni)

      END FUNCTION Mur_pt

!-----------------------------------------------------------------------

!>    Calculates electric field value for given location at spatial 
!!    domain boundary. Wrapper subroutine necessary to make universal 
!!    calls at x{min,max} points.
!!
!!    Method: Mur, 1st order Absorbing Boundary Conditions
!!
!!    @param[in] arr   electric field values at E(n)
!!    @param[in] arr1n electric field values at E(n-1)
!!    @param[in] x0    begin index of arr
!!    @param[in] pt    given boundary point x{min,max}
!!    @param[in] f     offset {+,-}1 for neighbouring point to x{min,max}
!!    @param[in] M     coefficient for calculation of 1st order Mur ABC

      SUBROUTINE calc_Mur_pt(arr, arr1n, x0, pt, f, M)

      integer, intent(in)    :: x0, pt, f
      real(p), intent(inout) :: arr(x0:)
      real(p), intent(in)    :: arr1n(x0:)
      real(p), intent(in)    :: M

      arr(pt) = Mur_pt(arr1n(pt+f), arr(pt+f), arr1n(pt), M)

      END SUBROUTINE calc_Mur_pt

!-----------------------------------------------------------------------

!>    Calculates electric field values at spatial domain boundaries.
!!    Wrapper subroutine making calls to 'calc_Mur_pt' to update 
!!    electric field values at x{min,max}, y{min,max} and z{min,max} points.
!!
!!    Method: Mur, 1st order Absorbing Boundary Conditions
!!
!!    @param[in] E               electric field components {x,y,z} at time step (n)
!!    @param[in] Eu1n_v{min,max} electric field components at time step (n-1), where u,v in {x,y,z}
!!    @param[in] imin, imax      spatial domain range indices over x-axis
!!    @param[in] jmin, jmax      spatial domain range indices over y-axis
!!    @param[in] kmin, kmax      spatial domain range indices over z-axis
!!    @param[in] Mx, My, Mz      coefficients for calculation of 1st
!!                               order Mur ABC over {x,y,z}-axes

      SUBROUTINE calc_Mur_ABC(E, Ex1n_ymin, Ex1n_ymax, Ex1n_zmin, &
                              Ex1n_zmax, Ey1n_xmin, Ey1n_xmax,    &
                              Ey1n_zmin, Ey1n_zmax, Ez1n_xmin,    &
                              Ez1n_xmax, Ez1n_ymin, Ez1n_ymax,    &
                              imin, imax, jmin, jmax, kmin, kmax, &
                              Mx, My, Mz)

      integer, intent(in)    :: imin, imax, jmin, jmax, kmin, kmax

      real(p), intent(inout) :: E(imin:,jmin:,kmin:,1:)

      real(p), intent(in) :: Ex1n_ymin(imin:,jmin:,kmin:), &
                             Ex1n_ymax(imin:,jmax:,kmin:), &
                             Ex1n_zmin(imin:,jmin:,kmin:), &
                             Ex1n_zmax(imin:,jmin:,kmax:)

      real(p), intent(in) :: Ey1n_xmin(imin:,jmin:,kmin:), &
                             Ey1n_xmax(imax:,jmin:,kmin:), &
                             Ey1n_zmin(imin:,jmin:,kmin:), &
                             Ey1n_zmax(imin:,jmin:,kmax:)

      real(p), intent(in) :: Ez1n_xmin(imin:,jmin:,kmin:), &
                             Ez1n_xmax(imax:,jmin:,kmin:), &
                             Ez1n_ymin(imin:,jmin:,kmin:), &
                             Ez1n_ymax(imin:,jmax:,kmin:)

      real(p),   intent(in) :: Mx, My, Mz

      ! Point coordinates
      integer :: i, j, k


      if (verbose_flag) then

        print *, "Calculating Mur ABC..."

      end if

      !$omp parallel do
      do k = kmin, kmax+1
        do i = imin, imax+1

          ! Calculate Ex @ y{min,max} boundary
          call calc_Mur_pt(E(i,jmin:jmin+1,k,1), Ex1n_ymin(i,:,k), jmin, jmin,   +1, My)
          call calc_Mur_pt(E(i,jmax:jmax+1,k,1), Ex1n_ymax(i,:,k), jmax, jmax+1, -1, My)

          ! Calculate Ez @ y{min,max} boundary
          call calc_Mur_pt(E(i,jmin:jmin+1,k,3), Ez1n_ymin(i,:,k), jmin, jmin,   +1, My)
          call calc_Mur_pt(E(i,jmax:jmax+1,k,3), Ez1n_ymax(i,:,k), jmax, jmax+1, -1, My)

        end do
      end do
      !$omp end parallel do


      !$omp parallel do
      do j = jmin, jmax+1
        do i = imin, imax+1

          ! Calculate Ex @ z{min,max} boundary
          call calc_Mur_pt(E(i,j,kmin:kmin+1,1), Ex1n_zmin(i,j,:), kmin, kmin,   +1, Mz)
          call calc_Mur_pt(E(i,j,kmax:kmax+1,1), Ex1n_zmax(i,j,:), kmax, kmax+1, -1, Mz)

          ! Calculate Ey @ z{min,max} boundary
          call calc_Mur_pt(E(i,j,kmin:kmin+1,2), Ey1n_zmin(i,j,:), kmin, kmin,   +1, Mz)
          call calc_Mur_pt(E(i,j,kmax:kmax+1,2), Ey1n_zmax(i,j,:), kmax, kmax+1, -1, Mz)

        end do
      end do
      !$omp end parallel do


      !$omp parallel do
      do k = kmin, kmax+1
        do j = jmin, jmax+1

          ! Calculate Ey @ x{min,max} boundary
          call calc_Mur_pt(E(imin:imin+1,j,k,2), Ey1n_xmin(:,j,k), imin, imin,   +1, Mx)
          call calc_Mur_pt(E(imax:imax+1,j,k,2), Ey1n_xmax(:,j,k), imax, imax+1, -1, Mx)

          ! Calculate Ez @ x{min,max} boundary
          call calc_Mur_pt(E(imin:imin+1,j,k,3), Ez1n_xmin(:,j,k), imin, imin,   +1, Mx)
          call calc_Mur_pt(E(imax:imax+1,j,k,3), Ez1n_xmax(:,j,k), imax, imax+1, -1, Mx)

        end do
      end do
      !$omp end parallel do

      END SUBROUTINE calc_Mur_ABC

!-----------------------------------------------------------------------

!>    Calculates coefficients for 1st, 2nd order Mur
!!    Absorbing Boundary Conditions using Yee's (2,2) or Fang's (2,4)
!!    discretisation scheme
!!
!!    @param[in]    C0  speed of light in vacuum
!!    @param[in]    ds  spatial  increment
!!    @param[in]    dt  temporal increment
!!    @param[in]    ord spatial discretisation order [2--Yee, 4--Fang]
!!    @param[inout] M   coefficients for calculation of 1st, 2nd order Mur ABC

      SUBROUTINE calc_Mur_coeffs(C0, ds, dt, ord, M)

      real(p), intent(in)    :: C0, ds, dt
      integer, intent(in)    :: ord
      real(p), intent(inout) :: M(2)

      real(p) :: alpha, omega


      alpha = C0 * dt

      select case (ord)

        case(2)  ! Yee's  (2,2) scheme

          omega = alpha + ds
  
          M(1) = (alpha-ds)/omega
          M(2) =  2.0d+0*ds/omega

        case(4)  ! Fang's (2,4) scheme

          omega = (24.0d+0*ds)/alpha

          M(1) = 27.0d+0 + omega
          M(2) = 27.0d+0 - omega

        case default

          print *, "Error: Incorrect order of spatial discretisation!"

      end select

      if (verbose_flag) then

        print *, "Mur ABC coefficients:", M

      end if

      END SUBROUTINE calc_Mur_coeffs

!-----------------------------------------------------------------------

!>    Re-arranges electric fields, i.e. saves given field
!!    values at current time step (n) as previous time step values (n-1).
!!
!!    @param[in] E, Eu1n_v{min,max} source and destination arrays
!!    @param[in] {i,j,k}{min,max}   domain ranges

      SUBROUTINE re_arrange(E, Ex1n_ymin, Ex1n_ymax, Ex1n_zmin, &
                            Ex1n_zmax, Ey1n_xmin, Ey1n_xmax,    &
                            Ey1n_zmin, Ey1n_zmax, Ez1n_xmin,    &
                            Ez1n_xmax, Ez1n_ymin, Ez1n_ymax,    &
                            imin, imax, jmin, jmax, kmin, kmax)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax

      real(p), intent(in) :: E(imin:,jmin:,kmin:,1:)

      real(p), intent(inout) :: Ex1n_ymin(imin:,jmin:,kmin:), &
                                Ex1n_ymax(imin:,jmax:,kmin:), &
                                Ex1n_zmin(imin:,jmin:,kmin:), &
                                Ex1n_zmax(imin:,jmin:,kmax:)

      real(p), intent(inout) :: Ey1n_xmin(imin:,jmin:,kmin:), &
                                Ey1n_xmax(imax:,jmin:,kmin:), &
                                Ey1n_zmin(imin:,jmin:,kmin:), &
                                Ey1n_zmax(imin:,jmin:,kmax:)

      real(p), intent(inout) :: Ez1n_xmin(imin:,jmin:,kmin:), &
                                Ez1n_xmax(imax:,jmin:,kmin:), &
                                Ez1n_ymin(imin:,jmin:,kmin:), &
                                Ez1n_ymax(imin:,jmax:,kmin:)


      if (verbose_flag) then

        print *, "Re-arranging electric field values..."

      end if

      ! Re-arrange Ex @ y{min,max}
      Ex1n_ymin(imin:imax+1, jmin:jmin+1, kmin:kmax+1) = &
              E(imin:imax+1, jmin:jmin+1, kmin:kmax+1, 1)

      Ex1n_ymax(imin:imax+1, jmax:jmax+1, kmin:kmax+1) = &
              E(imin:imax+1, jmax:jmax+1, kmin:kmax+1, 1)

      ! Re-arrange Ex @ z{min,max}
      Ex1n_zmin(imin:imax+1, jmin:jmin+1, kmin:kmin+1) = &
              E(imin:imax+1, jmin:jmin+1, kmin:kmin+1, 1)

      Ex1n_zmax(imin:imax+1, jmax:jmax+1, kmax:kmax+1) = &
              E(imin:imax+1, jmax:jmax+1, kmax:kmax+1, 1)


      ! Re-arrange Ey @ x{min,max}
      Ey1n_xmin(imin:imin+1, jmin:jmax+1, kmin:kmax+1) = &
              E(imin:imin+1, jmin:jmax+1, kmin:kmax+1, 2)

      Ey1n_xmax(imax:imax+1, jmin:jmax+1, kmin:kmax+1) = &
              E(imax:imax+1, jmin:jmax+1, kmin:kmax+1, 2)

      ! Re-arrange Ey @ z{min,max}
      Ey1n_zmin(imin:imax+1, jmin:jmax+1, kmin:kmin+1) = &
              E(imin:imax+1, jmin:jmax+1, kmin:kmin+1, 2)

      Ey1n_zmax(imin:imax+1, jmin:jmax+1, kmax:kmax+1) = &
              E(imin:imax+1, jmin:jmax+1, kmax:kmax+1, 2)


      ! Re-arrange Ez @ x{min,max}
      Ez1n_xmin(imin:imin+1, jmin:jmax+1, kmin:kmax+1) = &
              E(imin:imin+1, jmin:jmax+1, kmin:kmax+1, 3)

      Ez1n_xmax(imax:imax+1, jmin:jmax+1, kmin:kmax+1) = &
              E(imax:imax+1, jmin:jmax+1, kmin:kmax+1, 3)

      ! Re-arrange Ez @ y{min,max}
      Ez1n_ymin(imin:imax+1, jmin:jmin+1, kmin:kmax+1) = &
              E(imin:imax+1, jmin:jmin+1, kmin:kmax+1, 3)

      Ez1n_ymax(imin:imax+1, jmax:jmax+1, kmin:kmax+1) = &
              E(imin:imax+1, jmax:jmax+1, kmin:kmax+1, 3)

      END SUBROUTINE re_arrange

!-----------------------------------------------------------------------

      END MODULE MUR

!-----------------------------------------------------------------------
! EoF: mur.f90
!-----------------------------------------------------------------------
