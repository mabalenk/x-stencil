!-----------------------------------------------------------------------
!
!>    @brief Implements prototype of stencil calculation in 3D domain
!!           over given number of time iterations.
!!
!!    @author  Maksims Abalenkovs
!!    @email   m.abalenkovs@manchester.ac.uk
!!    @date    Apr 3, 2017
!!    @version 0.4
!!
!!    @brief Module for convenient usage of single, double, quad
!!           precision.
!
!-----------------------------------------------------------------------

      MODULE PRCSN

      USE, INTRINSIC :: ISO_FORTRAN_ENV

      ! Calculation precisions
      integer, parameter :: sp = REAL32
      integer, parameter :: dp = REAL64
      integer, parameter :: qp = REAL128
      integer, parameter ::  p = sp

      END MODULE PRCSN

!-----------------------------------------------------------------------
