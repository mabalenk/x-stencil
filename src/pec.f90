!-----------------------------------------------------------------------
!
!>    @brief Implements prototype of stencil calculation in 3D domain
!!           over given number of time iterations.
!!
!!    @author  Maksims Abalenkovs
!!    @email   m.abalenkovs@manchester.ac.uk
!!    @date    Sep 11, 2017
!!    @version 0.10
!!
!!    @brief Contains PEC/PMC enforcement routines
!!
!!    Subroutines (4):
!!
!!      enforce_pec4()
!!      stitch_pec4()
!!      enforce_pcm4()
!!      stitch_pcm4()
!
!-----------------------------------------------------------------------

      MODULE PEC

      USE PRCSN

      IMPLICIT NONE

!-----------------------------------------------------------------------

      CONTAINS

!-----------------------------------------------------------------------

      SUBROUTINE enforce_pec4(E, imin, imax, jmin, jmax, kmin, kmax)

      integer, intent(in)    :: imin, imax, jmin, jmax, kmin, kmax
      real(p), intent(inout) :: E(imin-1:, jmin-1:, kmin-1:, 1:)

      ! x{min,max}
      E(imin-1,:,:,:) = 0.0d+0
      E(imax+1,:,:,:) = 0.0d+0
      E(imax+2,:,:,:) = 0.0d+0

      ! y{min,max}
      E(:,jmin-1,:,:) = 0.0d+0
      E(:,jmax+1,:,:) = 0.0d+0
      E(:,jmax+2,:,:) = 0.0d+0

      ! z{min,max}
      E(:,:,kmin-1,:) = 0.0d+0
      E(:,:,kmax+1,:) = 0.0d+0
      E(:,:,kmax+2,:) = 0.0d+0

      END SUBROUTINE enforce_pec4

!-----------------------------------------------------------------------

      SUBROUTINE stitch_pec4(E, imin, imax, jmin, jmax, kmin, kmax)

      integer, intent(in)    :: imin, imax, jmin, jmax, kmin, kmax
      real(p), intent(inout) :: E(imin-1:, jmin-1:, kmin-1:, 1:)

      ! min
      E(imin-1,:,:,:) = E(imax,:,:,:)
      E(:,jmin-1,:,:) = E(:,jmax,:,:)
      E(:,:,kmin-1,:) = E(:,:,kmax,:)

      ! max
      E(imax+1,:,:,:) = E(imin,:,:,:)
      E(:,jmax+1,:,:) = E(:,jmin,:,:)
      E(:,:,kmax+1,:) = E(:,:,kmin,:)

      END SUBROUTINE stitch_pec4

!-----------------------------------------------------------------------

      SUBROUTINE enforce_pmc4(H, imin, imax, jmin, jmax, kmin, kmax)

      integer, intent(in)    :: imin, imax, jmin, jmax, kmin, kmax
      real(p), intent(inout) :: H(imin-2:, jmin-2:, kmin-2:, 1:)

      ! x{min,max}
      H(imin-2,:,:,:) = 0.0d+0
      H(imin-1,:,:,:) = 0.0d+0
      H(imax+1,:,:,:) = 0.0d+0

      ! y{min,max}
      H(:,jmin-2,:,:) = 0.0d+0
      H(:,jmin-1,:,:) = 0.0d+0
      H(:,jmax+1,:,:) = 0.0d+0

      ! z{min,max}
      H(:,:,kmin-2,:) = 0.0d+0
      H(:,:,kmin-1,:) = 0.0d+0
      H(:,:,kmax+1,:) = 0.0d+0

      END SUBROUTINE enforce_pmc4

!-----------------------------------------------------------------------

      SUBROUTINE stitch_pmc4(H, imin, imax, jmin, jmax, kmin, kmax)

      integer, intent(in)    :: imin, imax, jmin, jmax, kmin, kmax
      real(p), intent(inout) :: H(imin-2:, jmin-2:, kmin-2:, 1:)

      ! min
      H(imin-1,:,:,:) = H(imax,:,:,:)
      H(:,jmin-1,:,:) = H(:,jmax,:,:)
      H(:,:,kmin-1,:) = H(:,:,kmax,:)

      ! max
      H(imax+1,:,:,:) = H(imin,:,:,:)
      H(:,jmax+1,:,:) = H(:,jmin,:,:)
      H(:,:,kmax+1,:) = H(:,:,kmin,:)

      END SUBROUTINE stitch_pmc4

!-----------------------------------------------------------------------

      END MODULE PEC

!-----------------------------------------------------------------------
