!--------------------------------------------------------------------
!
!>    @brief Implements prototype of stencil calculation in 3D domain
!!           over given number of time iterations.
!!
!!    @author  Maksims Abalenkovs
!!    @email   maksims.abalenkovs@stfc.ac.uk
!!    @date    Oct 31, 2023
!!    @version 0.14
!!
!!    Subroutines (7):
!!
!!        check_file_act()
!!        open_out_file()
!!        close_out_file()
!!
!!        read_mat_specs()
!!        output_mat_coeffs()
!!        output_enc()
!!        print_headers()
!
!--------------------------------------------------------------------

      MODULE IO

      USE TYPES
      USE CONST
      USE VARIABLES

      IMPLICIT NONE
      SAVE

!--------------------------------------------------------------------

      CONTAINS

!--------------------------------------------------------------------

!>    Checks correct result of file action. Prints error message 
!!    and code on unsuccessful file action.
!!
!!    @param[in] act   verbal action description,
!!    @param[in] fname name of file
!!    @param[in] err   error code

      SUBROUTINE check_file_act(act, fname, err)

      character(len=*), intent(in) :: act
      character(len=*), intent(in) :: fname
      integer,          intent(in) :: err


      if (verbose_flag) then

          print *, "Checking '", trim(fname), "' ", act, "..."

      end if

      if (err /= 0) then
      
          print *, ""
          print *, "Error ", act, " file: '", trim(fname), "'!"
          print *, "Error code: ", err
          print *, ""
  
          stop

      end if

      END SUBROUTINE check_file_act

!-----------------------------------------------------------------------

!>    Opens output file
!!
!!    @param[in] fh    output file handle
!!    @param[in] fname output file name
!!    @param[in] fmt   header format
!!    @param[in] hdr   header

      SUBROUTINE open_out_file(fh, fname, fmt, hdr)

      integer,          intent(in) :: fh
      character(len=*), intent(in) :: fname
      character(len=*), intent(in) :: fmt
      character(len=*), intent(in) :: hdr

!     File existence
      logical :: exists = .false.

!     Error code
      integer :: err


      if (verbose_flag)  print *, "Opening output file..."

      inquire(file = fname, exist = exists)

      ! File exists
      if (exists) then
          
          open(unit    = fh,          &
               file    = fname,       &
               access  ="sequential", &
               form    ="formatted",  &
               status  ="old",        &
               position="append",     &
               iostat  = err)

      ! File does not exist (new file)
      else
          open(unit   = fh,          &
               file   = fname,       &
               access ="sequential", &
               form   ="formatted",  &
               status ="new",        &
               iostat = err)

          ! Write file header
          write (fh, fmt) hdr
      end if

!     Check correct file action
      call check_file_act("opening", fname, err)

      END SUBROUTINE open_out_file

!--------------------------------------------------------------------

!>    Closes output file
!!
!!    @param[in] fh    output file handle
!!    @param[in] fname output file name

      SUBROUTINE close_out_file(fh, fname)

      integer,          intent(in) :: fh
      character(len=*), intent(in) :: fname

!     Error code
      integer :: err


      if (verbose_flag) then

          print *,     "Closing output file..."

      end if

      close(unit=fh, iostat=err)

!     Check correct file action
      if (verbose_flag) then

          print *, "Checking '", fname, "' closing..."

      end if

      if (err /= 0) then
      
          print *, ""
          print *, "Error closing file: '", fname, "'!"
          print *, "Error code: ", err
          print *, ""
  
          stop

      end if

      END SUBROUTINE close_out_file

!--------------------------------------------------------------------

!>    @brief Reads simulation parameters from given file
!!
!!    @param[in] fh    input file handle
!!    @param[in] fname input file name

      SUBROUTINE read_sim_params(fh, fname)

      IMPLICIT NONE

      integer,          intent(in) :: fh
      character(len=*), intent(in) :: fname

!     Error code
      integer :: err

!     Dummy character
      character :: chr

!     Loop counters
      integer :: i


      print *, "Reading simulation parameters..."

      open(unit   = fh,    &
           file   = fname, &
           status ="old",  &
           iostat = err)

!     Check correct file action
      call check_file_act("opening", fname, err)

      read(fh, *) chr, imin, imax, jmin, jmax, kmin, kmax
      read(fh, *) chr, verbose_flag
      read(fh, *) chr, impulse_flag
      read(fh, *) chr, norm_flag
      read(fh, *) chr, pre_flag
      read(fh, *) chr, eqtype
      read(fh, *) chr, src_type
      read(fh, *) chr, abc_type
      read(fh, *) chr, nsrc

      allocate( src(nsrc), stat=err )

      do i = 1, nsrc
          read(fh, *) src(i)
      end do

      read(fh, *) chr, nobs

      allocate( obs(nobs), stat=err )

      do i = 1, nsrc
          read(fh, *) obs(i)
      end do

      read(fh, *) chr, tmax
      read(fh, *) chr, ncfl
      read(fh, *) chr, ord

      read(fh, *) chr, nmats
      read(fh, *) chr, resolution
      read(fh, *) chr, wfreq

      print *, "imin_kmax:   ", imin, imax, jmin, jmax, kmin, kmax
      print *, "verbose_flag:", verbose_flag
      print *, "impulse_flag:", impulse_flag
      print *, "norm_flag:   ", norm_flag
      print *, "pre_flag:    ", pre_flag
      print *, "eq_type:     ", eqtype
      print *, "src_type:    ", src_type
      print *, "abc_type:    ", abc_type
      print *, "nsrc:        ", nsrc

      do i = 1, nsrc
          print *, src(i)
      end do

      print *, "nobs:       ", nobs

      do i = 1, nsrc
          print *, obs(i)
      end do

      print *, "tmax:       ", tmax
      print *, "ncfl:       ", ncfl
      print *, "ord:        ", ord

      print *, "nmats:      ", nmats
      print *, "resolution: ", resolution
      print *, "wfreq:      ", wfreq
      print *, ""

!     Close input parameters file
      call close_out_file(fh, fname)

      END SUBROUTINE read_sim_params

!-----------------------------------------------------------------------

!>    Read pre-defined material specifications from file.
!!    Material specifications file maps encoded material code 'mcode' onto
!!    four media parameters
!!
!!    File structure:
!!        <mcode> -- material code,
!!        <mdesc> -- material verbal description,
!!        <sigma>, <eps_r>, <rho>, <mu_r> -- media parameters
!!
!!    @param[in] fh    material specification file handle
!!    @param[in] fname material specification file name

      SUBROUTINE read_mat_specs(fh, fname)

      integer,          intent(in) :: fh
      character(len=*), intent(in) :: fname

!     Output format specification
      character(len=128) :: frmt

!     Error code
      integer :: err

!     Loop counter
      integer :: i


      print *, "Reading material specifications..."

      open(unit   = fh,    &
           file   = fname, &
           status ="old",  &
           iostat = err)

!     Check correct file action
      call check_file_act("opening", fname, err)

!     Read material specifications
      do i = 1, nmats

          read (fh,*) mcode, mats(mcode)
          mats(mcode)%mdesc = trim(adjustl(mats(mcode)%mdesc))
  
          if (verbose_flag) then
  
              print '(1X, A18, I2.1, 1X, A47)', "Reading material: ", &
              mcode, mats(mcode)%mdesc
  
          end if

      end do

      call close_out_file(fh, fname)

!     Print read values
      print *, ""
      print "(A, I2)", "nmats: ", nmats
      print *, ""

      frmt = '(I2.1, X, A30, 3(5X, F11.8), 5X, ES15.8)'

      do i = 1, nmats

          print frmt, i, mats(i)

      end do

      print *, ""

      END SUBROUTINE read_mat_specs

!--------------------------------------------------------------------

!>    Outputs material coefficients for all materials
!!    Test subroutine
!!
!!    @param[in] fh    output file handle
!!    @param[in] fname output file name
!!    @param[in] G     electric field coefficients array
!!    @param[in] Z     magnetic field coefficient
!!    @param[in] nmats number of materials

      SUBROUTINE output_mat_coeffs(fh, fname, G, Z, nmats)

      integer,          intent(in) :: fh
      character(len=*), intent(in) :: fname
      real(p),          intent(in) :: G(1:,1:), Z(1:,1:)
      integer,          intent(in) :: nmats

!     Material counter
      integer :: v


      call open_out_file(fh, fname, "(A)", "# mcode | G(1) | G(2) | Z(1) | Z(2)")

      do v = 1, nmats

          write(fh, "(I3, 5E18.8E4)") v, G(v,:), Z(v,:)

      end do

      write(fh, *) ""

      call close_out_file(fh, fname)

      END SUBROUTINE output_mat_coeffs

!-----------------------------------------------------------------------

!>    Outputs material encoding locations array
!!    Test subroutine
!!
!!    @param[in] mats             materials array
!!    @param[in] enc              material encoding locations array
!!    @param[in] fname            output file name
!!    @param[in] fh               output file handle
!!    @param[in] {i,j,k}{min,max} dimensions of locations array

      SUBROUTINE output_enc(mats, enc, fh, fname,             &
                            imin, imax, jmin, jmax, kmin, kmax)

      type(material),   intent(in) :: mats(1:)
      integer,          intent(in) :: fh
      character(len=*), intent(in) :: fname
      integer,          intent(in) :: imin, imax, jmin, jmax, kmin, kmax
      integer,          intent(in) :: enc(imin:,jmin:,kmin:)

!     Point coordinates
      integer :: i, j, k


      call open_out_file(fh, fname, "(A)", "# i | j | k | mcode | mdesc | sigma | eps_r | rho | mu_r")

      do k = kmin, kmax
          do j = jmin, jmax
              do i = imin, imax

                  write(fh, "(3(I5.1), 1X, I2, 2X, A47, 4E18.8E4)") &
                        i, j, k, enc(i,j,k), mats(enc(i,j,k))

              end do
          end do
      end do

      call close_out_file(fh, fname)

      END SUBROUTINE output_enc

!-----------------------------------------------------------------------

!>    @brief Prints out 'headers' of {electric,magnetic} field arrays to screen

      SUBROUTINE print_headers(Ex, Ey, Ez, imin, jmin, kmin, delta)

      IMPLICIT NONE

      integer, intent(in) :: imin, jmin, kmin, delta

      real(p), intent(in) :: Ex(imin:,jmin:,kmin:), &
                             Ey(imin:,jmin:,kmin:), &
                             Ez(imin:,jmin:,kmin:)
      integer :: i


      do i = imin, imin+delta
          print *, Ex(i, jmin:jmin+delta, kmin)
      end do
      print *, ""

      do i = imin, imin+delta
          print *, Ey(i, jmin:jmin+delta, kmin)
      end do
      print *, ""

      do i = imin, imin+delta
          print *, Ez(i, jmin:jmin+delta, kmin)
      end do
      print *, ""

      END SUBROUTINE PRINT_HEADERS

!-----------------------------------------------------------------------

      END MODULE IO

!-----------------------------------------------------------------------
!!    @eof io.f90
!-----------------------------------------------------------------------
