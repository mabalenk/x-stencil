!-----------------------------------------------------------------------
!
!>    @brief Implements prototype of stencil calculation in 3D domain
!!           over given number of time iterations.
!!
!!    @author  Maksims Abalenkovs
!!    @email   maksims.abalenkovs@stfc.ac.uk
!!    @date    Oct 31, 2023
!!    @version 0.14
!
!-----------------------------------------------------------------------

      PROGRAM X_STENCIL

!-----------------------------------------------------------------------

      USE CONST
      USE FANG
      USE GAUSSIAN
      USE INIT
      USE IO
      USE MUR
      USE MUR_FANG
      USE OMP_LIB
      USE PEC
      USE TYPES
      USE VARIABLES
      USE YEE

      IMPLICIT NONE

!-----------------------------------------------------------------------

      ! Loop counter
      integer :: i

!-----------------------------------------------------------------------

      call read_sim_params(51, "input_params")

      call init_sim

      ! @test Print out 'headers' of electromagnetic field arrays
      print *, "Headers of electric field arrays E{x,y,z}:"
      call print_headers(E(:,:,:,1), E(:,:,:,2), E(:,:,:,3), &
                         imin, jmin, kmin, 5)

      print *, "Headers of magnetic field arrays H{x,y,z}:"
      call print_headers(H(:,:,:,1), H(:,:,:,2), H(:,:,:,3), &
                         imin-1, jmin-1, kmin-1, 5)

      call init_radio_env

      ! @test Print out 'header' of material encodings
      print *, "Header of material encodings:"

      do i = imin, imin+5
        print *, enc(i, jmin:jmin+5, 1)
      end do
      print *, ""

      call calc_mat_coeffs(dt, iota, nmats, G, Z)

      ! @test Print out 'headers' of FDTD coefficients
      print *, "Headers of FDTD coefficients Gamma, Zeta:"

      do i = 1, min(nmats,5)
        print *, G(i,1:2)
      end do
      print *, ""

      do i = 1, min(nmats,5)
        print *, Z(i,1:2)
      end do
      print *, ""

      if (pre_flag) then

        call build_coeff_mtrx(imin, imax, jmin, jmax, kmin, kmax, &
                              enc, ord, G, Z, Gm, Zm, 51, 52,     &
                             "gamma.coef", "zeta.coef")
      end if

      if (impulse_flag) then

        Jz(n0) = 1.0d+0

      else

        call generate_gaussian(Jz, dt, n0, wfreq)

      end if

      ! Enter main loop on time
      do t = 1, tmax

          if ((abc_type == "mur1").or.(abc_type == "mur2")) then
  
              select case (ord)
    
                  case(2)
      
                      call re_arrange(E, Ex1n_ymin, Ex1n_ymax, Ex1n_zmin, Ex1n_zmax, &
                                         Ey1n_xmin, Ey1n_xmax, Ey1n_zmin, Ey1n_zmax, &
                                         Ez1n_xmin, Ez1n_xmax, Ez1n_ymin, Ez1n_ymax, &
                                      imin, imax, jmin, jmax, kmin, kmax)
                  case(4)
      
                      call re_arrange_H4(H, Hx1n_ymin, Hx1n_ymax, Hx1n_zmin, Hx1n_zmax, &
                                            Hy1n_xmin, Hy1n_xmax, Hy1n_zmin, Hy1n_zmax, &
                                            Hz1n_xmin, Hz1n_xmax, Hz1n_ymin, Hz1n_ymax, &
                                         imin, imax, jmin, jmax, kmin, kmax)
        
                      call re_arrange_E4(E, Ex1n_ymin, Ex1n_ymax, Ex1n_zmin, Ex1n_zmax, &
                                            Ey1n_xmin, Ey1n_xmax, Ey1n_zmin, Ey1n_zmax, &
                                            Ez1n_xmin, Ez1n_xmax, Ez1n_ymin, Ez1n_ymax, &
                                         imin, imax, jmin, jmax, kmin, kmax)
              end select
  
          end if

        t0   = omp_get_wtime()

        select case (ord)

          case(2)       ! 2nd order

            if (pre_flag) then
    
              call calc_H_star_pre(H, E, eqtype, Zm, imin, imax,     &
                                   jmin, jmax, kmin, kmax, dx, dy, dz)
    
              call calc_E_star_pre(E, H, eqtype, Gm, imin, imax,     &
                                   jmin, jmax, kmin, kmax, dx, dy, dz)
            else
    
              call calc_H_star(H, E, eqtype, Z, enc, imin, imax, &
                               jmin, jmax, kmin, kmax, dx, dy, dz)
    
              call calc_E_star(E, H, eqtype, G, enc, imin, imax, &
                               jmin, jmax, kmin, kmax, dx, dy, dz)
            end if

            if (abc_type == "mur1") then

              call calc_Mur_ABC(E, Ex1n_ymin, Ex1n_ymax, Ex1n_zmin, Ex1n_zmax, &
                                   Ey1n_xmin, Ey1n_xmax, Ey1n_zmin, Ey1n_zmax, &
                                   Ez1n_xmin, Ez1n_xmax, Ez1n_ymin, Ez1n_ymax, &
                                   imin, imax, jmin, jmax, kmin, kmax,         &
                                   Mx(1), My(1), Mz(1))
            end if

          case(4)       ! 4th order

            if (pre_flag) then
    
              call calc_H4_star_pre(H, E, eqtype, Zm, imin, imax,     &
                                    jmin, jmax, kmin, kmax, dx, dy, dz)

              call enforce_pmc4(H, imin, imax, jmin, jmax, kmin, kmax)
              call stitch_pmc4(H, imin, imax, jmin, jmax, kmin, kmax)

              if (abc_type == "mur1") then

                call calc_Mur_ABC_H4(H, Hx1n_ymin, Hx1n_ymax, Hx1n_zmin, Hx1n_zmax, &
                                        Hy1n_xmin, Hy1n_xmax, Hy1n_zmin, Hy1n_zmax, &
                                        Hz1n_xmin, Hz1n_xmax, Hz1n_ymin, Hz1n_ymax, &
                                        imin, imax, jmin, jmax, kmin, kmax,         &
                                        Mx(1), My(1), Mz(1))
              end if

              call calc_E4_star_pre(E, H, eqtype, Gm, imin, imax,     &
                                    jmin, jmax, kmin, kmax, dx, dy, dz)

              if (abc_type == "mur1") then

                call calc_Mur_ABC_E4(E, Ex1n_ymin, Ex1n_ymax, Ex1n_zmin, Ex1n_zmax, &
                                        Ey1n_xmin, Ey1n_xmax, Ey1n_zmin, Ey1n_zmax, &
                                        Ez1n_xmin, Ez1n_xmax, Ez1n_ymin, Ez1n_ymax, &
                                        imin, imax, jmin, jmax, kmin, kmax,         &
                                        Mx(1), My(1), Mz(1))
              end if

              call enforce_pec4(E, imin, imax, jmin, jmax, kmin, kmax)
              call stitch_pec4(E, imin, imax, jmin, jmax, kmin, kmax)

            else

              call calc_H4_star(H, E, eqtype, Z, enc, ord, imin, imax, &
                               jmin, jmax, kmin, kmax, dx, dy, dz)

              call calc_E4_star(E, H, eqtype, G, enc, ord, imin, imax, &
                                jmin, jmax, kmin, kmax, dx, dy, dz)
            end if

          case default  ! Error message

            stop "Error: Incorrect order of spatial discretisation!"

        end select

        t1   = omp_get_wtime()
        time = time+t1-t0

        if (pre_flag) then
  
          call calc_E_src_pre(E(:,:,:,3), "electric", imin, jmin, kmin,  &
                              ord/2-1, nsrc, src, src_type, Gm, Jz, n0, t)
        else
  
          call calc_E_src(E(:,:,:,3), "electric", imin, jmin, kmin,      &
                          ord/2-1, nsrc, src, src_type, G, enc, Jz, n0, t)
        end if

!       Output H{x,y,z}a, E{x,y,z}a values
        write (oh,"(6(3X,ES10.3))") H(obs(1)%x,obs(1)%y,obs(1)%z,1), &
                                    H(obs(1)%x,obs(1)%y,obs(1)%z,2), &
                                    H(obs(1)%x,obs(1)%y,obs(1)%z,3), &
                                    E(obs(1)%x,obs(1)%y,obs(1)%z,1), &
                                    E(obs(1)%x,obs(1)%y,obs(1)%z,2), &
                                    E(obs(1)%x,obs(1)%y,obs(1)%z,3)
      end do


      ! @test Print out 'headers' of electromagnetic field arrays
      print *, "Headers of electric field arrays E{x,y,z}:"
      call print_headers(E(:,:,:,1), E(:,:,:,2), E(:,:,:,3), &
                         imin, jmin, kmin, 5)

      print *, "Headers of magnetic field arrays H{x,y,z}:"
      call print_headers(H(:,:,:,1), H(:,:,:,2), H(:,:,:,3), &
                         imin-1, jmin-1, kmin-1, 5)

      ! Output OpenMP computation time
      write (th, "(I3.1,F10.2)") nthr, time

      ! Finalise simulation
      call fin_sim

!-----------------------------------------------------------------------

      END PROGRAM X_STENCIL

!-----------------------------------------------------------------------
!!    @eof x-stencil.f90
!-----------------------------------------------------------------------
