!-----------------------------------------------------------------------
!
!>    @brief Contains implementation of 3D FDTD stencil calculation
!!           routines using Fang's 4th order spatial approximations.
!!
!!    @author  Maksims Abalenkovs
!!    @email   m.abalenkovs@manchester.ac.uk
!!    @date    Sep 28, 2017
!!    @version 0.14
!!
!!    Subroutines (4):
!!
!!      calc_E4_star()
!!      calc_E4_star_pre()
!!      calc_H4_star()
!!      calc_H4_star_pre()
!!
!!    Functions (4):
!!
!!      calc_E4()
!!      calc_E4_pre()
!!      calc_H4()
!!      calc_H4_pre()
!
!-----------------------------------------------------------------------

      MODULE FANG

      USE IO
      USE ALLOC
      USE VARIABLES

      IMPLICIT NONE

!-----------------------------------------------------------------------

      CONTAINS

!-----------------------------------------------------------------------

!>    Calculates electric field values at current time step (n)
!!    for entire spatial domain using 4th order spatial approx.
!!    Wrapper subroutine.
!!
!!    @param[in,out] E               electric field components x, y, z
!!    @param[in]     H               magnetic field components x, y, z
!!    @param[in]     eqtype          equation type (expl, func, dotp, matm)
!!    @param[in]     G               coefficients array, G(nmats,2)
!!    @param[in]     enc             material encoding locations array
!!    @param[in]     ord             approximation order for space derivatives
!!    @param[in]    {i,j,k}{min,max} simulation domain boundaries
!!    @param[in]     dx, dy, dz      spatial increments

      SUBROUTINE calc_E4_star(E, H, eqtype, G, enc, ord, imin, imax, &
                              jmin, jmax, kmin, kmax, dx, dy, dz)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax, ord
      integer, target, intent(in) :: enc(imin:,jmin:,kmin:)

      real(p), intent(inout) :: E(imin-1:,jmin-1:,kmin-1:,1:)
      real(p), intent(in)    :: H(imin-2:,jmin-2:,kmin-2:,1:)

      real(p), intent(in) :: G(1:,1:), dx, dy, dz

      character(len=4), intent(in) :: eqtype

!     Point coordinates
      integer :: i = 0, j = 0, k = 0

!     Material code
      integer, pointer :: mc

!     Vector of coefficients
      real(p) :: u(ord+1)


      if (verbose_flag) then

        print *, "Calculating electric field values..."

      end if

!     Calculate E{x,y,z}

      select case (eqtype)

        case ("func")  ! func fly

          if (verbose_flag)  print *, "Eq. type: func"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          E(i,j,k,1) = calc_E4(G(enc(i,j,k),:),  E(i,j,k,1), &
                               H(i,j-2:j+1,k,3), dy,         &
                               H(i,j,k-2:k+1,2), dz)
    
          E(i,j,k,2) = calc_E4(G(enc(i,j,k),:),  E(i,j,k,2), &
                               H(i,j,k-2:k+1,1), dz,         &
                               H(i-2:i+1,j,k,3), dx)
    
          E(i,j,k,3) = calc_E4(G(enc(i,j,k),:),  E(i,j,k,3), &
                               H(i-2:i+1,j,k,2), dx,         &
                               H(i,j-2:j+1,k,1), dy)
              end do
            end do
          end do
          !$omp end parallel do

        case ("dotp")  ! dot prod fly

          if (verbose_flag)  print *, "Eq. type: dotp"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax
          
          mc =>  enc(i,j,k)

          u  =  [G(mc,1), real(27,p)/24*G(mc,2), -G(mc,2)/24, &
                         -real(27,p)/24*G(mc,2),  G(mc,2)/24]

          E(i,j,k,1) = dot_product(u, [E(i,j,k,1),                    &
                                      (H(i,j,  k,3)-H(i,j-1,k,3))/dy, &
                                      (H(i,j+1,k,3)-H(i,j-2,k,3))/dy, &
                                      (H(i,j,  k,2)-H(i,j,k-1,2))/dz, &
                                      (H(i,j,k+1,2)-H(i,j,k-2,2))/dz])

          E(i,j,k,2) = dot_product(u, [E(i,j,k,2),                    &
                                      (H(i,j,k,  1)-H(i,j,k-1,1))/dz, &
                                      (H(i,j,k+1,1)-H(i,j,k-2,1))/dz, &
                                      (H(i,  j,k,3)-H(i-1,j,k,3))/dx, &
                                      (H(i+1,j,k,3)-H(i-2,j,k,3))/dx])

          E(i,j,k,3) = dot_product(u, [E(i,j,k,3),                    &
                                      (H(i,  j,k,2)-H(i-1,j,k,2))/dx, &
                                      (H(i+1,j,k,2)-H(i-2,j,k,2))/dx, &
                                      (H(i,j,  k,1)-H(i,j-1,k,1))/dy, &
                                      (H(i,j+1,k,1)-H(i,j-2,k,1))/dy])
              end do
            end do
          end do
          !$omp end parallel do

        case ("matm")  ! matm fly

          if (verbose_flag)  print *, "Eq. type: matm"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax
          
          mc =>  enc(i,j,k)

          u  =  [G(mc,1), real(27,p)/24*G(mc,2), -G(mc,2)/24, &
                         -real(27,p)/24*G(mc,2),  G(mc,2)/24]

          E(i,j,k,:) = matmul(u,                                               &
                         reshape([E(i,j,k,1), (H(i,j,  k,3)-H(i,j-1,k,3))/dy,  &
                                              (H(i,j+1,k,3)-H(i,j-2,k,3))/dy,  &
                                              (H(i,j,  k,2)-H(i,j,k-1,2))/dz,  &
                                              (H(i,j,k+1,2)-H(i,j,k-2,2))/dz,  &
                                  E(i,j,k,2), (H(i,j,k,  1)-H(i,j,k-1,1))/dz,  &
                                              (H(i,j,k+1,1)-H(i,j,k-2,1))/dz,  &
                                              (H(i,  j,k,3)-H(i-1,j,k,3))/dx,  &
                                              (H(i+1,j,k,3)-H(i-2,j,k,3))/dx,  &
                                  E(i,j,k,3), (H(i,  j,k,2)-H(i-1,j,k,2))/dx,  &
                                              (H(i+1,j,k,2)-H(i-2,j,k,2))/dx,  &
                                              (H(i,j,  k,1)-H(i,j-1,k,1))/dy,  &
                                              (H(i,j+1,k,1)-H(i,j-2,k,1))/dy], &
                                 [ord+1, 3]))
              end do
            end do
          end do
          !$omp end parallel do

        case default  ! expl fly

          if (verbose_flag)  print *, "Eq. type: expl"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          E(i,j,k,1) = G(enc(i,j,k),1) * E(i,j,k,1) +                          &
            G(enc(i,j,k),2)/24/dy *                                            &
              ( 27*(H(i,j,k,3)-H(i,j-1,k,3)) - (H(i,j+1,k,3)-H(i,j-2,k,3)) ) - &
            G(enc(i,j,k),2)/24/dz *                                            &
              ( 27*(H(i,j,k,2)-H(i,j,k-1,2)) - (H(i,j,k+1,2)-H(i,j,k-2,2)) )
    
          E(i,j,k,2) = G(enc(i,j,k),1) * E(i,j,k,2) +                          &
            G(enc(i,j,k),2)/24/dz *                                            &
              ( 27*(H(i,j,k,1)-H(i,j,k-1,1)) - (H(i,j,k+1,1)-H(i,j,k-2,1)) ) - &
            G(enc(i,j,k),2)/24/dx *                                            &
              ( 27*(H(i,j,k,3)-H(i-1,j,k,3)) - (H(i+1,j,k,3)-H(i-2,j,k,3)) )
    
          E(i,j,k,3) = G(enc(i,j,k),1) * E(i,j,k,3) +                          &
            G(enc(i,j,k),2)/24/dx *                                            &
              ( 27*(H(i,j,k,2)-H(i-1,j,k,2)) - (H(i+1,j,k,2)-H(i-2,j,k,2)) ) - &
            G(enc(i,j,k),2)/24/dy *                                            &
              ( 27*(H(i,j,k,1)-H(i,j-1,k,1)) - (H(i,j+1,k,1)-H(i,j-2,k,1)) )

              end do
            end do
          end do
          !$omp end parallel do

        end select

      END SUBROUTINE calc_E4_star

!-----------------------------------------------------------------------

!>    Calculates electric field values at current time step (n)
!!    for entire spatial domain using 4th order spatial approx.
!!    Wrapper subroutine.
!!
!!    @note Uses pre-computed coefficidents Gamma in matrix form
!!
!!    @param[in,out] E               electric field components x, y, z
!!    @param[in]     H               magnetic field components x, y, z
!!    @param[in]     eqtype          equation type (expl,func,dotp,ddot,dgmv,matm,dgmm)
!!    @param[in]     Gm              coefficients in matrix form, Gm(i,j,k,2)
!!    @param[in]    {i,j,k}{min,max} simulation domain boundaries
!!    @param[in]     d{x,y,z}        spatial increments

      SUBROUTINE calc_E4_star_pre(E, H, eqtype, Gm, imin, imax, &
                                  jmin, jmax, kmin, kmax, dx, dy, dz)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax

      real(p), intent(inout) :: E(imin-1:,jmin-1:,kmin-1:,1:)
      real(p), intent(in)    :: H(imin-2:,jmin-2:,kmin-2:,1:)

      real(p),   intent(in) :: Gm(imin:,jmin:,kmin:,1:), dx, dy, dz

      character(len=4), intent(in) :: eqtype

!     Point coordinates
      integer :: i, j, k


      if (verbose_flag) then

        print *, "Calculating electric field values (E4 pre)..."

      end if

!     Calculate E{x,y,z}

      select case (eqtype)

        case ("func")  ! func pre

          if (verbose_flag)  print *, "Eq. type: func"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          E(i,j,k,1) = calc_E4_pre(Gm(i,j,k,:), E(i,j,k,1), &
                                    H(i,j-2:j+1,k,3), dy,   &
                                    H(i,j,k-2:k+1,2), dz)
    
          E(i,j,k,2) = calc_E4_pre(Gm(i,j,k,:), E(i,j,k,2), &
                                    H(i,j,k-2:k+1,1), dz,   &
                                    H(i-2:i+1,j,k,3), dx)
    
          E(i,j,k,3) = calc_E4_pre(Gm(i,j,k,:), E(i,j,k,3), &
                                    H(i-2:i+1,j,k,2), dx,   &
                                    H(i,j-2:j+1,k,1), dy)
              end do
            end do
          end do
          !$omp end parallel do

        case ("dotp")  ! dot prod pre

          if (verbose_flag)  print *, "Eq. type: dotp"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax
          
          E(i,j,k,1) = dot_product(Gm(i,j,  k,  :), [E(i,j,k,1),       &
                                   (H(i,j,  k,  3)-H(i,j-1,k,  3))/dy, &
                                   (H(i,j+1,k,  3)-H(i,j-2,k,  3))/dy, &
                                   (H(i,j,  k,  2)-H(i,j,  k-1,2))/dz, &
                                   (H(i,j,  k+1,2)-H(i,j,  k-2,2))/dz])

          E(i,j,k,2) = dot_product(Gm(i,  j,k,  :), [E(i,j,k,2),       &
                                   (H(i,  j,k,  1)-H(i,  j,k-1,1))/dz, &
                                   (H(i,  j,k+1,1)-H(i,  j,k-2,1))/dz, &
                                   (H(i,  j,k,  3)-H(i-1,j,k,  3))/dx, &
                                   (H(i+1,j,k,  3)-H(i-2,j,k,  3))/dx])

          E(i,j,k,3) = dot_product(Gm(i,j,k,:), [E(i,j,k,3),           &
                                   (H(i,  j,  k,2)-H(i-1,j,  k,2))/dx, &
                                   (H(i+1,j,  k,2)-H(i-2,j,  k,2))/dx, &
                                   (H(i,  j,  k,1)-H(i,  j-1,k,1))/dy, &
                                   (H(i,  j+1,k,1)-H(i,  j-2,k,1))/dy])
              end do
            end do
          end do
          !$omp end parallel do

        case ("matm")  ! matm pre

          if (verbose_flag)  print *, "Eq. type: matm"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax
          
          E(i,j,k,:) = matmul(Gm(i,j,k,:),                                     &
                         reshape([E(i,j,k,1), (H(i,j,  k,3)-H(i,j-1,k,3))/dy,  &
                                              (H(i,j+1,k,3)-H(i,j-2,k,3))/dy,  &
                                              (H(i,j,  k,2)-H(i,j,k-1,2))/dz,  &
                                              (H(i,j,k+1,2)-H(i,j,k-2,2))/dz,  &
                                  E(i,j,k,2), (H(i,j,k,  1)-H(i,j,k-1,1))/dz,  &
                                              (H(i,j,k+1,1)-H(i,j,k-2,1))/dz,  &
                                              (H(i,j,k,  3)-H(i-1,j,k,3))/dx,  &
                                              (H(i+1,j,k,3)-H(i-2,j,k,3))/dx,  &
                                  E(i,j,k,3), (H(i,  j,k,2)-H(i-1,j,k,2))/dx,  &
                                              (H(i+1,j,k,2)-H(i-2,j,k,2))/dx,  &
                                              (H(i,  j,k,1)-H(i,j-1,k,1))/dy,  &
                                              (H(i,j+1,k,1)-H(i,j-2,k,1))/dy], &
                                 [ord+1, 3]))
              end do
            end do
          end do
          !$omp end parallel do

        case default  ! expl pre

          if (verbose_flag)  print *, "Eq. type: expl"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          E(i,j,k,1) = Gm(i,j,k,1) * E(i,j,k,1) +                    &
              ( Gm(i,j,k,2) * (H(i,j,  k,  3)-H(i,j-1,k,  3)) +      &
                Gm(i,j,k,3) * (H(i,j+1,k,  3)-H(i,j-2,k,  3)) )/dy + &
              ( Gm(i,j,k,4) * (H(i,j,  k,  2)-H(i,j,  k-1,2)) +      &
                Gm(i,j,k,5) * (H(i,j,  k+1,2)-H(i,j,  k-2,2)) )/dz
    
          E(i,j,k,2) = Gm(i,j,k,1) * E(i,j,k,2) +                    &
              ( Gm(i,j,k,2) * (H(i,  j,k,  1)-H(i,  j,k-1,1)) +      &
                Gm(i,j,k,3) * (H(i,  j,k+1,1)-H(i,  j,k-2,1)) )/dz + &
              ( Gm(i,j,k,4) * (H(i,  j,k,  3)-H(i-1,j,k,  3)) +      &
                Gm(i,j,k,5) * (H(i+1,j,k,  3)-H(i-2,j,k,  3)) )/dx
    
          E(i,j,k,3) = Gm(i,j,k,1) * E(i,j,k,3) +                    &
              ( Gm(i,j,k,2) * (H(i,  j,  k,2)-H(i-1,j,  k,2)) +      &
                Gm(i,j,k,3) * (H(i+1,j,  k,2)-H(i-2,j,  k,2)) )/dx + &
              ( Gm(i,j,k,4) * (H(i,  j,  k,1)-H(i,  j-1,k,1)) +      &
                Gm(i,j,k,5) * (H(i,  j+1,k,1)-H(i,  j-2,k,1)) )/dy

              end do
            end do
          end do
          !$omp end parallel do

        end select

      END SUBROUTINE calc_E4_star_pre

!-----------------------------------------------------------------------

!>    Calculates magnetic field values at current time step (n)
!!    for entire spatial domain using 4th order spatial approx.
!!    Wrapper subroutine.
!!
!!    @param[in,out] H               magnetic field components x, y, z
!!    @param[in]     E               electric field components x, y, z
!!    @param[in]     eqtype          equation type (expl,func,dotp,ddot,dgmv,matm,dgmm)
!!    @param[in]     Z               coefficients array, Z(nmats,2)
!!    @param[in]     enc             material encoding locations array
!!    @param[in]     ord             approximation order for space derivatives
!!    @param[in]    {i,j,k}{min,max} simulation domain boundaries
!!    @param[in]     dx, dy, dz      spatial increments

      SUBROUTINE calc_H4_star(H, E, eqtype, Z, enc, ord, imin, imax, &
                              jmin, jmax, kmin, kmax, dx, dy, dz)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax, ord
      integer, target, intent(in) :: enc(imin:,jmin:,kmin:)

      real(p), intent(inout) :: H(imin-2:,jmin-2:,kmin-2:,1:)
      real(p), intent(in)    :: E(imin-1:,jmin-1:,kmin-1:,1:)

      real(p),   intent(in) :: Z(1:,1:), dx, dy, dz

      character(len=4), intent(in) :: eqtype

!     Point coordinates
      integer :: i = 0, j = 0, k = 0

!     Material code
      integer, pointer :: mc

!     Vector of coefficients
      real(p) :: u(ord+1)


      if (verbose_flag) then

        print *, "Calculating magnetic field values..."

      end if

!     Calculate H{x,y,z}

      select case (eqtype)

        case ("func")  ! func fly

          if (verbose_flag)  print *, "Eq. type: func"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = calc_H4(Z(enc(i,j,k),:), H(i,j,k,1), &
                               E(i,j,k-1:k+2,2), dz,        &
                               E(i,j-1:j+2,k,3), dy)
    
          H(i,j,k,2) = calc_H4(Z(enc(i,j,k),:), H(i,j,k,2), &
                               E(i-1:i+2,j,k,3), dx,        &
                               E(i,j,k-1:k+2,1), dz)
    
          H(i,j,k,3) = calc_H4(Z(enc(i,j,k),:), H(i,j,k,3), &
                               E(i,j-1:j+2,k,1), dy,        &
                               E(i-1:i+2,j,k,2), dx)
              end do
            end do
          end do
          !$omp end parallel do

        case ("dotp")  ! dot prod fly

          if (verbose_flag)  print *, "Eq. type: dotp"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          mc =>  enc(i,j,k)

          u  =  [Z(mc,1), real(27,p)/24*Z(mc,2), -Z(mc,2)/24, &
                         -real(27,p)/24*Z(mc,2),  Z(mc,2)/24]

          H(i,j,k,1) = dot_product(u, [H(i,j,k,1),                    &
                                      (E(i,j,k+1,2)-E(i,j,k,  2))/dz, &
                                      (E(i,j,k+2,2)-E(i,j,k-1,2))/dz, &
                                      (E(i,j+1,k,3)-E(i,j,  k,3))/dy, &
                                      (E(i,j+2,k,3)-E(i,j-1,k,3))/dy])
    
          H(i,j,k,2) = dot_product(u, [H(i,j,k,2),                    &
                                      (E(i+1,j,k,3)-E(i,  j,k,3))/dx, &
                                      (E(i+2,j,k,3)-E(i-1,j,k,3))/dx, &
                                      (E(i,j,k+1,1)-E(i,j,k,  1))/dz, &
                                      (E(i,j,k+2,1)-E(i,j,k-1,1))/dz])
    
          H(i,j,k,3) = dot_product(u, [H(i,j,k,3),                    &
                                      (E(i,j+1,k,1)-E(i,j,  k,1))/dy, &
                                      (E(i,j+2,k,1)-E(i,j-1,k,1))/dy, &
                                      (E(i+1,j,k,2)-E(i,  j,k,2))/dx, &
                                      (E(i+2,j,k,2)-E(i-1,j,k,2))/dx])
              end do
            end do
          end do
          !$omp end parallel do

        case ("matm")  ! matm fly

          if (verbose_flag)  print *, "Eq. type: matm"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          mc =>  enc(i,j,k)

          u  =  [Z(mc,1),  real(27,p)/24*Z(mc,2), -Z(mc,2)/24, &
                          -real(27,p)/24*Z(mc,2),  Z(mc,2)/24]

          H(i,j,k,:) = matmul(u,                                               &
                         reshape([H(i,j,k,1), (E(i,j,k+1,2)-E(i,j,k,  2))/dz,  &
                                              (E(i,j,k+2,2)-E(i,j,k-1,2))/dz,  &
                                              (E(i,j+1,k,3)-E(i,j,  k,3))/dy,  &
                                              (E(i,j+2,k,3)-E(i,j-1,k,3))/dy,  &
                                  H(i,j,k,2), (E(i+1,j,k,3)-E(i,  j,k,3))/dx,  &
                                              (E(i+2,j,k,3)-E(i-1,j,k,3))/dx,  &
                                              (E(i,j,k+1,1)-E(i,j,k,  1))/dz,  &
                                              (E(i,j,k+2,1)-E(i,j,k-1,1))/dz,  &
                                  H(i,j,k,3), (E(i,j+1,k,1)-E(i,j,  k,1))/dy,  &
                                              (E(i,j+2,k,1)-E(i,j-1,k,1))/dy,  &
                                              (E(i+1,j,k,2)-E(i,  j,k,2))/dx,  &
                                              (E(i+2,j,k,2)-E(i-1,j,k,2))/dx], &
                                 [ord+1, 3]))
              end do
            end do
          end do
          !$omp end parallel do

        case default  ! expl fly

          if (verbose_flag)  print *, "Eq. type: expl"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = Z(enc(i,j,k),1) * H(i,j,k,1) +                          &
            Z(enc(i,j,k),2)/24/dz *                                            &
              ( 27*(E(i,j,k+1,2)-E(i,j,k,2)) - (E(i,j,k+2,2)-E(i,j,k-1,2)) ) - &
            Z(enc(i,j,k),2)/24/dy *                                            &
              ( 27*(E(i,j+1,k,3)-E(i,j,k,3)) - (E(i,j+2,k,3)-E(i,j-1,k,3)) )
    
          H(i,j,k,2) = Z(enc(i,j,k),1) * H(i,j,k,2) +                          &
            Z(enc(i,j,k),2)/24/dx *                                            &
              ( 27*(E(i+1,j,k,3)-E(i,j,k,3)) - (E(i+2,j,k,3)-E(i-1,j,k,3)) ) - &
            Z(enc(i,j,k),2)/24/dz *                                            &
              ( 27*(E(i,j,k+1,1)-E(i,j,k,1)) - (E(i,j,k+2,1)-E(i,j,k-1,1)) )
    
          H(i,j,k,3) = Z(enc(i,j,k),1) * H(i,j,k,3) +                          &
            Z(enc(i,j,k),2)/24/dy *                                            &
              ( 27*(E(i,j+1,k,1)-E(i,j,k,1)) - (E(i,j+2,k,1)-E(i,j-1,k,1)) ) - &
            Z(enc(i,j,k),2)/24/dx *                                            &
              ( 27*(E(i+1,j,k,2)-E(i,j,k,2)) - (E(i+2,j,k,2)-E(i-1,j,k,2)) )

              end do
            end do
          end do
          !$omp end parallel do

      end select

      END SUBROUTINE calc_H4_star

!-----------------------------------------------------------------------

!>    Calculates magnetic field values at current time step (n)
!!    for entire spatial domain using 4th order spatial approx.
!!    Wrapper subroutine.
!!
!!    @note Uses pre-computed coefficidents Zeta in matrix form
!!
!!    @param[in,out] H               magnetic field components x, y, z
!!    @param[in]     E               electric field components x, y, z
!!    @param[in]     eqtype          equation type (expl,func,dotp,ddot,dgmv,matm,dgmm)
!!    @param[in]     Zm              coefficients in matrix form, Zm(i,j,k,2)
!!    @param[in]    {i,j,k}{min,max} simulation domain boundaries
!!    @param[in]     d{x,y,z}        spatial increments

      SUBROUTINE calc_H4_star_pre(H, E, eqtype, Zm, imin, imax,     &
                                  jmin, jmax, kmin, kmax, dx, dy, dz)

      integer, intent(in) :: imin, imax, jmin, jmax, kmin, kmax

      real(p), intent(inout) :: H(imin-2:,jmin-2:,kmin-2:,1:)
      real(p), intent(in)    :: E(imin-1:,jmin-1:,kmin-1:,1:)

      real(p),   intent(in) :: Zm(imin:,jmin:,kmin:,1:), dx, dy, dz

      character(len=4), intent(in) :: eqtype

!     Point coordinates
      integer :: i, j, k


      if (verbose_flag) then

        print *, "Calculating magnetic field values (H4 pre)..."

      end if

!     Calculate H{x,y,z}

      select case (eqtype)

        case ("func")  ! func pre

          if (verbose_flag)  print *, "Eq. type: func"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = calc_H4_pre(Zm(i,j,k,:), H(i,j,k,1), &
                                    E(i,j,k-1:k+2,2), dz,   &
                                    E(i,j-1:j+2,k,3), dy)
    
          H(i,j,k,2) = calc_H4_pre(Zm(i,j,k,:), H(i,j,k,2), &
                                    E(i-1:i+2,j,k,3), dx,   &
                                    E(i,j,k-1:k+2,1), dz)
    
          H(i,j,k,3) = calc_H4_pre(Zm(i,j,k,:), H(i,j,k,3), &
                                    E(i,j-1:j+2,k,1), dy,   &
                                    E(i-1:i+2,j,k,2), dx)
              end do
            end do
          end do
          !$omp end parallel do

        case ("dotp")  ! dot prod pre

          if (verbose_flag)  print *, "Eq. type: dotp"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = dot_product(Zm(i,j,  k,  :), [H(i,j,k,1),       &
                                   (E(i,j,  k+1,2)-E(i,j,  k,  2))/dz, &
                                   (E(i,j,  k+2,2)-E(i,j,  k-1,2))/dz, &
                                   (E(i,j+1,k,  3)-E(i,j,  k,  3))/dy, &
                                   (E(i,j+2,k,  3)-E(i,j-1,k,  3))/dy])
    
          H(i,j,k,2) = dot_product(Zm(i,  j,k,  :), [H(i,j,k,  2),     &
                                   (E(i+1,j,k,  3)-E(i,  j,k,  3))/dx, &
                                   (E(i+2,j,k,  3)-E(i-1,j,k,  3))/dx, &
                                   (E(i,  j,k+1,1)-E(i,  j,k,  1))/dz, &
                                   (E(i,  j,k+2,1)-E(i,  j,k-1,1))/dz])
    
          H(i,j,k,3) = dot_product(Zm(i,  j,  k,:), [H(i,j,  k,3),     &
                                   (E(i,  j+1,k,1)-E(i,  j,  k,1))/dy, &
                                   (E(i,  j+2,k,1)-E(i,  j-1,k,1))/dy, &
                                   (E(i+1,j,  k,2)-E(i,  j,  k,2))/dx, &
                                   (E(i+2,j,  k,2)-E(i-1,j,  k,2))/dx])
              end do
            end do
          end do
          !$omp end parallel do

        case ("matm")  ! matm pre

          if (verbose_flag)  print *, "Eq. type: matm"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,:) = matmul(Zm(i,j,k,:),                                     &
                         reshape([H(i,j,k,1), (E(i,j,k+1,2)-E(i,j,k,  2))/dz,  &
                                              (E(i,j,k+2,2)-E(i,j,k-1,2))/dz,  &
                                              (E(i,j+1,k,3)-E(i,j,  k,3))/dy,  &
                                              (E(i,j+2,k,3)-E(i,j-1,k,3))/dy,  &
                                  H(i,j,k,2), (E(i+1,j,k,3)-E(i,  j,k,3))/dx,  &
                                              (E(i+2,j,k,3)-E(i-1,j,k,3))/dx,  &
                                              (E(i,j,k+1,1)-E(i,j,k,  1))/dz,  &
                                              (E(i,j,k+2,1)-E(i,j,k-1,1))/dz,  &
                                  H(i,j,k,3), (E(i,j+1,k,1)-E(i,j,  k,1))/dy,  &
                                              (E(i,j+2,k,1)-E(i,j-1,k,1))/dy,  &
                                              (E(i+1,j,k,2)-E(i,  j,k,2))/dx,  &
                                              (E(i+2,j,k,2)-E(i-1,j,k,2))/dx], &
                                 [ord+1, 3]))
              end do
            end do
          end do
          !$omp end parallel do

        case default  ! expl pre

          if (verbose_flag)  print *, "Eq. type: expl"

          !$omp parallel do
          do k = kmin, kmax
            do j = jmin, jmax
              do i = imin, imax

          H(i,j,k,1) = Zm(i,j,k,1) * H(i,j,k,1) +                    &
              ( Zm(i,j,k,2) * (E(i,j,  k+1,2)-E(i,j,  k,  2)) +      &
                Zm(i,j,k,3) * (E(i,j,  k+2,2)-E(i,j,  k-1,2)) )/dz + &
              ( Zm(i,j,k,4) * (E(i,j+1,k,  3)-E(i,j,  k,  3)) +      &
                Zm(i,j,k,5) * (E(i,j+2,k,  3)-E(i,j-1,k,  3)) )/dy
    
          H(i,j,k,2) = Zm(i,j,k,1) * H(i,j,k,2) +                    &
              ( Zm(i,j,k,2) * (E(i+1,j,k,  3)-E(i,  j,k,  3)) +      &
                Zm(i,j,k,3) * (E(i+2,j,k,  3)-E(i-1,j,k,  3)) )/dx + &
              ( Zm(i,j,k,4) * (E(i,  j,k+1,1)-E(i,  j,k,  1)) +      &
                Zm(i,j,k,5) * (E(i,  j,k+2,1)-E(i,  j,k-1,1)) )/dz
    
          H(i,j,k,3) = Zm(i,j,k,1) * H(i,j,k,3) +                    &
              ( Zm(i,j,k,2) * (E(i,  j+1,k,1)-E(i,  j,  k,1)) +      &
                Zm(i,j,k,3) * (E(i,  j+2,k,1)-E(i,  j-1,k,1)) )/dy + &
              ( Zm(i,j,k,4) * (E(i+1,j,  k,2)-E(i,  j,  k,2)) +      &
                Zm(i,j,k,5) * (E(i+2,j,  k,2)-E(i-1,j,  k,2)) )/dx

              end do
            end do
          end do
          !$omp end parallel do

      end select

      END SUBROUTINE calc_H4_star_pre

!-----------------------------------------------------------------------

!>    Calculates electric field value E at time step (n)
!!    for given spatial location (i,j) using 4th order spatial approx.
!!
!!    @param[in] G       coefficients
!!    @param[in] E       electric field at time step (n-1)
!!    @param[in] H{1,2}  magnetic field at time step (n)
!!    @param[in] ds{1,2} spatial increment over x,y axes

      real(p) FUNCTION calc_E4(G, E, H1, ds1, H2, ds2)

      IMPLICIT NONE

      real(p), intent(in) :: G(2), E, H1(4), ds1, H2(4), ds2

      calc_E4 = G(1)*E + G(2)/24/ds1*( 27*(H1(3)-H1(2)) - (H1(4)-H1(1)) ) - &
                         G(2)/24/ds2*( 27*(H2(3)-H2(2)) - (H2(4)-H2(1)) )

      END FUNCTION calc_E4

!-----------------------------------------------------------------------

!>    Calculates electric field value E at time step (n)
!!    for given spatial location (i,j) using 4th order spatial approx.
!!
!!    @note Uses pre-computed coefficidents Gamma in matrix form
!!
!!    @param[in] Gm      coefficients in matrix form
!!    @param[in] E       electric field at time step (n-1)
!!    @param[in] H{1,2}  magnetic field at time step (n)
!!    @param[in] ds{1,2} spatial increments over x,y,z axes

      real(p) FUNCTION calc_E4_pre(Gm, E, H1, ds1, H2, ds2)

      IMPLICIT NONE

      real(p), intent(in) :: Gm(5), E, H1(4), ds1, H2(4), ds2

      calc_E4_pre = Gm(1)*E + (Gm(2)*(H1(3)-H1(2)) + Gm(3)*(H1(4)-H1(1)))/ds1 + &
                              (Gm(4)*(H2(3)-H2(2)) + Gm(5)*(H2(4)-H2(1)))/ds2

      END FUNCTION calc_E4_pre

!-----------------------------------------------------------------------

!>    Calculates magnetic field value H at time step (n+1)
!!    for given spatial location (i,j) using 4th order spatial approx.
!!
!!    @param[in] Z       coefficients
!!    @param[in] H       magnetic field at time step (n)
!!    @param[in] E{1,2}  electric field at time step (n)
!!    @param[in] ds{1,2} spatial increments over x,y,z axes

      real(p) FUNCTION calc_H4(Z, H, E1, ds1, E2, ds2)

      IMPLICIT NONE

      real(p), intent(in) :: Z(2), H, E1(4), ds1, E2(4), ds2

      calc_H4 = Z(1)*H + Z(2)/24/ds1*( 27*(E1(3)-E1(2)) - (E1(4)-E1(1)) ) - &
                         Z(2)/24/ds2*( 27*(E2(3)-E2(2)) - (E2(4)-E2(1)) )

      END FUNCTION calc_H4

!-----------------------------------------------------------------------

!>    Calculates magnetic field value H at time step (n+1)
!!    for given spatial location (i,j) using 4th order spatial approx.
!!
!!    @note Uses pre-computed coefficidents Zeta in matrix form
!!
!!    @param[in] Zm      coefficients in matrix form
!!    @param[in] H       magnetic field at time step (n)
!!    @param[in] E{1,2}  electric field at time step (n)
!!    @param[in] ds{1,2} spatial increments over x,y,z axes

      real(p) FUNCTION calc_H4_pre(Zm, H, E1, ds1, E2, ds2)

      IMPLICIT NONE

      real(p), intent(in) :: Zm(5), H, E1(4), ds1, E2(4), ds2

      calc_H4_pre = Zm(1)*H + (Zm(2)*(E1(3)-E1(2)) + Zm(3)*(E1(4)-E1(1)))/ds1 + &
                              (Zm(4)*(E2(3)-E2(2)) + Zm(5)*(E2(4)-E2(1)))/ds2

      END FUNCTION calc_H4_pre

!-----------------------------------------------------------------------

      END MODULE FANG

!-----------------------------------------------------------------------
